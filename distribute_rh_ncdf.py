'''
Created on Nov 8, 2016
Script mod'd from matlab script to run dk to  distirbute early relative humidity at rcew
@author: pkormos
'''

import numpy as np
import netCDF4 as nc
from pandas import DataFrame, offsets
import matplotlib.pyplot as plt
import os
import subprocess as sp 

wyoi = np.arange(1999, 2000)  # water years of interest
outdir = '/Volumes/megadozer/CZO/' # define output dir
os.chdir(outdir) # goto directory
PATH = os.environ.copy()["PATH"] + ':/usr/local/bin:/Users/pkormos/bin:'

# bring in relative humidity data 
rnc = '/Volumes/megadozer/CZO/nc_working_files/rcew_rh_pk.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r')            # create precip ncdf file
tt = rn.variables['time']
time = nc.num2date(tt[:], tt.units)
rvar = rn.variables["relative_humidity"]
rxyz = rn.variables["easting_northing_elev"]
rh_st = rn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(rvar)[1])]
r_list = [rh_st.getncattr(tt_list[k]) for k in range(np.shape(rvar)[1])] # sorted rh station list
rh = DataFrame(np.ma.filled(rvar[:,0:21],np.nan),index=time,columns=r_list[0:21]) # make pandas dataset
rhxyz = DataFrame(rxyz[:],index = r_list,columns = ['x','y','z'])
rn.close()
rh.index = rh.index-offsets.Hour(-7)    # fix time back to local time


## loop through water years to distribute rh
for y in wyoi: # for each year
    # find continuous blocks of data
    tt0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y].dropna(axis=1,how='all')
    
    # take out stations D03 and RMSP for entire year 1984-1998
    if np.sum(np.arange(1984,1999)==y)>0:
        del tt0['rc.usc-d03']
        del tt0['rc.tg.rme-rmsp']
    
    # WY1999, rmsp comes in for some times, D03 needs to come out for the whole year. see time frames from below
    if y==1999:
        del tt0['rc.usc-d03']
        tt0.ix['1998-10-1':'1998-10-27 10:00',['rc.tg.rme-rmsp']] = np.nan
        tt0.ix['1999-07-8 06:00':'1999-07-14 13:00',['rc.tg.rme-rmsp']] = np.nan
        tt0.ix['1999-09-17 03:00':'1999-10-1 00:00',['rc.tg.rme-rmsp']] = np.nan
        tt0 = tt0.astype(float)
        
    # WY2000, 10-01-99 00:00 to 10-06-99 9:00, take out rmsp. It's just copied from 176.
    if y == 2000:
        del tt0['rc.usc-d03']
        tt0.ix['1999-10-1':'1999-10-6 9:00',['rc.tg.rme-rmsp']] = np.nan
        
    # take out D03 for entire year 2001:2014
    if np.sum(np.arange(2001,2015)==y)>0:
        del tt0['rc.usc-d03']
        
    # build data input file for DK %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ttd1 = np.isfinite(tt0)                       # find changes in missing data
    ttd2 = 1*10**np.arange(0,np.shape(tt0)[1])    # build unique addative index
    ttd3 = ttd1 * np.tile(ttd2,[ttd1.shape[0],1]) # repeat and mult.
    ttd4 = ttd3.sum(1)                            # add by rows
    ttd5 = np.where(ttd4.diff()!=0)[0]            # find changes
    ttds = np.append(ttd5,tt0.shape[0])           # get starting and ending indexes
    outdir2   = '%sworking_dir/'%outdir     # output directory (current directory is where all the files go.

    for d in np.arange(0,ttd5.shape[0]):                      # for each missing data divide
        dat_block = tt0.ix[ttds[d]:ttds[d+1]].dropna(axis=1,how='all') # get rh data
        datfile = '%sdk_in_wy%i_%i'%(outdir2,y,d) + '.txt' # build filename
        fmtstring = "%i %i " + '%5.2f ' * (dat_block.shape[1]) # build the format string
        firstline = str(dat_block.shape[1]) + ' 1 ' + str(ttds[d]+1) + ' '  + str(ttds[d+1]) + '\n' # build the first line 
        xyzlines = str(''.join([dat_block.columns[i] + ' ' + str(rhxyz.ix[tt0.columns[i]].z) + ' ' + str(rhxyz.ix[tt0.columns[i]].y) + ' ' + str(rhxyz.ix[tt0.columns[i]].x) + '\n' for i in np.arange(0,dat_block.shape[1])]))
        xyzlines = xyzlines[:-1] # get rid of the \n on the end
        hdr = firstline + xyzlines # build the comment
        db = np.zeros([dat_block.shape[0],dat_block.shape[1]+2],dtype=float) # mem
        db[:,0] = np.tile(y,[dat_block.shape[0],1]).ravel()                  # put in the water years
        db[:,1] = np.transpose(np.matrix(np.arange(ttds[d]+1,ttds[d+1]+1))).ravel() # put in the timestep
        db[:,2:] = np.matrix(dat_block) # put in the data
        np.savetxt(datfile,db,delimiter=' ',fmt=fmtstring,header=hdr,comments='') # write the file
               
        dem_file = '/Users/pkormos/rcew_met_dist/terrain/rcew_10m.txt'
        msk_file = '/Users/pkormos/rcew_met_dist/terrain/rcew_10m_msk.txt'
         
    for d in np.arange(0,ttd5.shape[0]):                      # for each missing data divide
        #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        # build configuration file for DK, configured to run within wy directory
        #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        # user input **********************************************
        out_file = '%srh_dk_out%i_%i.txt'%(outdir2,y,d)
        datfile = outdir2 + 'dk_in_wy' + '%i_%i'%(y,d) + '.txt' # build filename
        # end user input ******************************************
        
        conffile = outdir2 + 'dk_config_wy%i_%i.txt'%(y,d)
        fid = open(conffile,'w');                               # open file for writing
        fid.write('%s\n'%('input-data-file-name=' + datfile))   # from previous cell block
        fid.write('%s\n'%'type-of-data=4')                      # 1 precip, 2 temp, 3 swe, 4 other (no restrictions on detrending)
        fid.write('%s\n'%'time-step=1')                         # 1=hourly; 2=daily; 3=monthly; 4=yearly
        fid.write('%s\n'%'coord-system=4')                      # 4 for easting and northing, ARC/INFO grid format
        fid.write('%s\n'%('elevation-grid-file-name=' + dem_file))
        fid.write('%s\n'%('watershed-mask-file-name=' + msk_file))
        fid.write('%s\n'%'zone-grid-file-name=')                # leave empty unless we find an inversion
        fid.write('%s\n'%'output-format=5')                     # 5 for daily fields (dk481only)-- netcdf format plus mean areal values tabular
        fid.write('%s\n'%('beginning-period-number=%i'%(ttds[d]+1)))
        fid.write('%s\n'%('ending-period-number=%i'%ttds[d+1]))
        fid.write('%s\n'%'output-precision=1')                  # 1 for floating point, one decimal place
        fid.write('%s\n'%('output-file-name=' + out_file))
        fid.write('%s\n'%'zone-output-file-name=')              # leave empty
        fid.write('%s\n'%'regression-method=2')                 # 1 for least squares 2 for least absolute deviations.
        fid.write('%s\n'%'station-weighting-method=1')          # 1=distance (kriging); 2=equal weights
        fid.write('%s\n'%'timesteps-per-period=1')              # run for each timestep
        fid.write('%s\n'%'input-format-csv=false')              # Command-line switch option for OMS-csv input format: if true,
        fid.write('%s\n'%'print-input=false')
        fid.write('%s\n'%'print-distances=false')
        fid.write('%s\n'%'print-regressions=true')
        fid.write('%s\n'%'print-residuals=false')
        fid.write('%s\n'%'print-weights=false')
        #     fid.write('%s\n'%'output-rounding=2')
        fid.close()
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # run dk
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    os.chdir(outdir2)
    for d in np.arange(0,ttd5.shape[0]):                      # for each missing data divide
        print('working on wy%i, file %i of %i\n'%(y,d+1, ttd5.shape[0]))
        cmd = 'dk -t 5 -k %s/dk_config_wy%i_%i.txt'%(outdir2,y,d) 
        proc = sp.Popen(cmd, shell=True, env={"PATH": PATH}).wait() # run dk
        

