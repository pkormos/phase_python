###########################################################################
# script to trouble shoot dew point grids 1991
#
# created - Pat Kormos November 2016
###########################################################################

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
# import os.path
# import pandas as pd
# import progressbar
 
y=1991
dpt_file = "/Volumes/megadozer/CZO/dp_corrected/dp_wy%s.nc" % y

# get dewpoint netcdf file pointers
d_ncid = nc.Dataset(dpt_file,'r')                   # open ncdf file
d_varid = d_ncid.variables['dp']                    # get dp var id
fig0 = plt.figure(num=10)
for t in np.arange(3,88):                                       
    tt_d = d_varid[350:375,500,500].squeeze()       
    type(tt_d)
    nnn = np.isfinite(tt_d)
    plt.close()
    fig0 = plt.figure(num=10)
    ax1 = fig0.add_subplot(2,1,1)
    plt.plot(tt_d)
#     plt.title('timesteps %s+'%(t*100))
    ax2 = fig0.add_subplot(2,1,2)
    plt.plot(nnn)
    plt.show()
    plt.pause(0.05)
    print('first %s steps seem fine?'%((t+1)*100))
d_ncid.close()                                      # close ncdf file
