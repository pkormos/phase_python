###########################################################################
# script to evaluate the relative humidity during storms for early years.
# WY1984 - 1992. plots early histograms and saves them (11x17)
# 
# created - Pat Kormos October 2016
###########################################################################

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
from pandas import DataFrame, date_range

wyoi = np.arange(1984, 2014)  # water years of interest

# bring in precipitation file
p_list = np.array(['rc-047', 'rc-055', 'rc-059', 'rc-074', 'rc-075', 'rc-076', 'rc-078', 'rc-083', # manually bring in column headers
    'rc-088', 'rc-095b', 'rc-097', 'rc-106', 'rc-114', 'rc-119', 'rc-128', 'rc.fl-057',
    'rc.lsc-127', 'rc.mc-043041', 'rc.mc-053', 'rc.mc-054', 'rc.mc-061', 'rc.mc-072',
    'rc.ng-098c', 'rc.ng-108', 'rc.sc-012', 'rc.sc-015', 'rc.sc-023', 'rc.sc-024',
    'rc.sc-031','rc.sc-045', 'rc.sc.mp-033', 'rc.sum-049', 'rc.tg-116c','rc.tg-126',
    'rc.tg-145', 'rc.tg-147', 'rc.tg-155', 'rc.tg-156', 'rc.tg-165', 'rc.tg-167',
    'rc.tg-174_ppt', 'rc.tg.dc-144', 'rc.tg.dc-154', 'rc.tg.dc-163', 'rc.tg.dc-163',
    'rc.tg.dc.jd-124', 'rc.tg.dc.jd-124b', 'rc.tg.dc.jd-125', 'rc.tg.rme-166b',
    'rc.tg.rme-176', 'rc.tg.rme-rmsp', 'rc.tg.rmw-166x94','rc.usc-138031', 
    'rc.usc-138044', 'rc.usc-d03','rc.usc-j10', 'rc.usc-l21'])
pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
pn = nc.Dataset(pnc, 'r')            # create precip ncdf file
pvar = pn.variables["precipitation"]
ppt = DataFrame(pvar[:], index=date_range('1962-1-1', '2014-12-31 23:00', freq='H'), columns = p_list) # make pandas dataset
pn.close()

# set some plot values
fig = plt.figure(num=1, figsize=(11,17), dpi=100, facecolor='w')

# set some bar chart values
b_edg = np.linspace(0, 1, num=51)       # bin edges
width = 0.02


# # plot humidity histogram (during precipitation) from WY1984-1991
# for y in np.nditer(wyoi[0:8]):					# can put 7 x 4 on an 11x17 paper
#     print "begin WY%s" % y
#       
#     rh_file = "/Volumes/LaCie/rh_cf/rh_wy%s.nc" % y
#           
#     # get stations with rh
#     rh_ncid = nc.Dataset(rh_file,'r')		# id for rh file
#     rh_st = rh_ncid.variables['stations']	# id for stations
#     rh_rd = rh_ncid.variables['raw_data']   # id for raw data
#     atlist = ["station_%s" % i for i in range(rh_rd.shape[1])]
#     stlist = [rh_st.getncattr(atlist[k]) for k in range(rh_rd.shape[1])] # sorted rh station list
#     c_stns = np.intersect1d(p_list,stlist)                               # list of common station names
#       
#     p = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get precipitation time slice of all station
#     p = p[c_stns]                                   # keep only precip data for stations with rh
#     rh = rh_rd[:,np.in1d(stlist,p_list)]   # common rh data
#   
#     if __name__ == '__main__':
#         plt.ion()
#         for s in range(np.shape(p)[1]):
#             ind = np.array(p[c_stns[s]]>0)
#             wts = p[c_stns[s]]
#             wts = wts[ind]
#             whst = np.histogram(rh[ind,s],bins = b_edg,weights=wts)  # get weighted histogram bar heights
#             if c_stns[s] == 'rc-076':
#                 sp = (y-1984)*4+1 
#             if c_stns[s] == 'rc.lsc-127':
#                 sp = (y-1984)*4+2 
#             if c_stns[s] == 'rc.usc-j10':
#                 sp = (y-1984)*4+3 
#             if c_stns[s] == 'rc.tg.rme-176':
#                 sp = (y-1984)*4+4 
#             ax = fig.add_subplot(8,4,sp)
#             rects1 = ax.bar(whst[1][0:np.size(whst[0])]+.01, whst[0], width, color='green')
#             plt.axis([0,1.01,0,plt.ylim()[1]])
#             if s==0:
#                 plt.ylabel("WY%s" % y)
#             if y==1984:
#                 plt.title(c_stns[s])
#             if y==1984 and s==0:
#                 plt.text(1.45,43,'rc.lsc-127',size=14)
#             plt.text(.1, 15, "max=%s" % np.max(rh[ind,s]))
#             plt.grid(True)
#             plt.show()
#             raw_input("Press [enter] to continue.") # wait for input from the user
# #             plt.close()
#           
# # fig.savefig('wt_rh_hist_1.tif', dpi=100)



