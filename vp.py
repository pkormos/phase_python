'''
Created on Oct 27, 2015
Modified 11/15/2016 by pat kormos in file restructuring

Bring in air temperature and relative humidity to calculate vapor pressure and 
dew point temperature. uses not rounded rh and ta.

@author: pkormos
'''

import subprocess as sp 
import os
import numpy as np
# import matplotlib.pyplot as plt
import netCDF4 as nc
from isnobal import ipw
import progressbar

#===============================================================================
# IPW output file
#===============================================================================
IPW = "/Users/pkormos/ipw/bin"
PATH = os.environ.copy()["PATH"] + ':/usr/local/bin:' + IPW
vp_dir = '/Volumes/megadozer/CZO/vp_corrected/'
dp_dir = '/Volumes/megadozer/CZO/dp_corrected/'
ta_dir = '/Volumes/SHARE1/Mac_Files/team-files/Patrick/ta_not_rounded/'
rh_dir = '/Volumes/megadozer/CZO/rh_corrected/'
tmp_dir = '/Users/pkormos/scratch/'
msk_file = '/Users/pkormos/rcew_met_dist/terrain/msk010.ipw'

# geo header info from ################################
# history = text2ipw -l 2813 -s 1395 -b 1 msk.t 
# history = mkgeoh -o 4795634.73,511405.27 -c UTM -u meters -d -10,10 msk.ip
 
u = 4795634.73
v = 511405.27
du = -10
dv = 10
units = 'meters'
csys = 'UTM'
nbits = 16

## loop through water years
wyoi = np.arange(1999, 2000)  # water years of interest
for y in wyoi:
    print('begin ' + str(y))
    ta_ncdfFile = ta_dir + 'ta_wy%s'%y + '.nc'
    rh_ncdfFile = rh_dir + 'rh_wy%s'%y + '.nc'
    vp_ncdfFile = vp_dir + 'vp_wy%s'%y + '.nc'
    dp_ncdfFile = dp_dir + 'dp_wy%s'%y + '.nc'

    tn = nc.Dataset(ta_ncdfFile, 'r') # input ta ncdf file open
    rn = nc.Dataset(rh_ncdfFile, 'r') # input rh ncdf file open 
    vn = nc.Dataset(vp_ncdfFile, 'w') # output vp ncdf file create
    dn = nc.Dataset(dp_ncdfFile, 'w') # output dp ncdf file
        
    ## work on vp netcdf file
    # copy dimensions over from ta to vp netcdf file
    tdim = tn.dimensions['time']
    vn.createDimension(tdim.name,0)
    ydim = tn.dimensions['y']
    vn.createDimension(ydim.name,len(ydim))
    xdim = tn.dimensions['x']
    vn.createDimension(xdim.name,len(xdim))

    # copy variables over from ta to vp netcdf file
    tvar = tn.variables['time']
    tvar_vp = vn.createVariable(tvar.name,tvar.datatype,tvar.dimensions) # make same tvar in vp
    tvar_vp[:] = tvar[:] # add data to variable
    yvar = tn.variables['y'] # get yvar
    yvar_vp = vn.createVariable(yvar.name,yvar.datatype,yvar.dimensions) # put that yvar in vp
    yvar_vp[:] = yvar[:] # put y coords in there
    xvar = tn.variables['x'] # get xvar
    xvar_vp = vn.createVariable(xvar.name,xvar.datatype,xvar.dimensions) # make same xvar in vp
    xvar_vp[:] = xvar[:] # put x coords in there
    vp_var = vn.createVariable('vp','f4',('time','y','x')) # create vp variable
    # vp_var = vn.variables['vp']
    setattr(vn.variables['vp'], 'units', 'Pa')
    setattr(vn.variables['vp'], 'long_name', 'vapor pressure in degrees Pascals')
    
    # Copy variable attributes
    tvar_vp.setncatts({k: tvar.getncattr(k) for k in tvar.ncattrs()})
    yvar_vp.setncatts({k: yvar.getncattr(k) for k in yvar.ncattrs()})
    xvar_vp.setncatts({k: xvar.getncattr(k) for k in xvar.ncattrs()})
    
    ## work on dp netcdf file
    # copy dimensions over from ta to dp netcdf file
    dn.createDimension(tdim.name,0)
    dn.createDimension(ydim.name,len(ydim))
    dn.createDimension(xdim.name,len(xdim))

    # copy variables over from ta to vp netcdf file
    tvar_dp = dn.createVariable(tvar.name,tvar.datatype,tvar.dimensions) # make same tvar in vp
    tvar_dp[:] = tvar[:] # add data to variable
    yvar_dp = dn.createVariable(yvar.name,yvar.datatype,yvar.dimensions) # put that yvar in vp
    yvar_dp[:] = yvar[:] # put y coords in there
    xvar_dp = dn.createVariable(xvar.name,xvar.datatype,xvar.dimensions) # make same xvar in vp
    xvar_dp[:] = xvar[:] # put x coords in there
    dp_var = dn.createVariable('dp','f4',('time','y','x')) # create vp variable
    # dp_var = dn.variables['dp']
    setattr(dn.variables['dp'], 'units', 'C')
    setattr(dn.variables['dp'], 'long_name', 'dew point temperature in degrees Celcius')
    
    # Copy variable attributes
    tvar_dp.setncatts({k: tvar.getncattr(k) for k in tvar.ncattrs()})
    yvar_dp.setncatts({k: yvar.getncattr(k) for k in yvar.ncattrs()})
    xvar_dp.setncatts({k: xvar.getncattr(k) for k in xvar.ncattrs()})

    
    timesteps = range(0,len(tvar[:]))                  # timesteps to loop over
    pbar = progressbar.ProgressBar(max_value = len(timesteps)).start()
    for ts in timesteps:                    # for ts in timeStep:
        ta = tn.variables['ta'][ts,:,:]      # get the air temp grid
        rh = rn.variables['relative_humidity'][ts,:,:]      # get the relative humidity
        i = ipw.IPW()                       # create ipw space
        i.new_band(rh)                      # put rh in band 0
        i.new_band(ta)                      # put ta in band 1
        i.add_geo_hdr([u, v], [du, dv], units, csys) # add geo header
        i.write('%s%s' % (tmp_dir,  'irh2vp_in.ipw'), nbits) # write the file
        
        cmd = 'irh2vp -c -m ' + msk_file + ' < ' + tmp_dir + 'irh2vp_in.ipw  > ' + tmp_dir + 'vp.ipw' # build irh2vp command 
        proc = sp.Popen(cmd, shell=True, env={"PATH": PATH,"IPW": IPW}).wait() # run irh2vp
        
        cmd2 = 'idewpt -m ' + msk_file + ' -P 4 -t .01 ' + tmp_dir + 'vp.ipw > ' + tmp_dir + 'dp.ipw'
        proc2 = sp.Popen(cmd2,shell=True, env={"PATH": PATH,"IPW": IPW}).wait() # run idewpt
        
        i = ipw.IPW(tmp_dir + 'vp.ipw') # read output from irh2vp
        vp_var[ts,:,:] = i.bands[0].data
        i = ipw.IPW(tmp_dir + 'dp.ipw') # read output from irh2vp
        #         print(timesteps)
        #         plt.imshow(i.bands[0].data)
        #         plt.show()
        dp_var[ts,:,:] = i.bands[0].data
        if np.mod(ts,100) == 0:
            vn.sync()
            dn.sync()
        pbar.update(ts)
    pbar.finish()
vn.close()
dn.close()
tn.close()
rn.close()
print('done with '+ str(y))

