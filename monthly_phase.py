###########################################################################
# scrip to summarize output files from ta_td_analysis.m into monthly
# numbers.
# Pat Kormos November 2015
# modified 12/5/2016 to do both dew point and wet bulb.
###########################################################################
import netCDF4 as nc
import numpy as np
from pandas import DataFrame

wyoi = np.arange(1984, 2014)  # water years of interest
wym = np.roll(np.arange(1984,2015),3)
msk = np.genfromtxt('/Users/pkormos/rcew_met_dist/terrain/subwatershed_masks/shdmsk010.txt', delimiter=' ',skip_header=6)

# hflag = "d" # for using dewpoint temperaturxe
hflag = "w" # for using wetbulb temperature


def dp_pctsnow(hvar):
    pctsnow = np.zeros(hvar.shape[1],hvar.shape[2]) # mem
    pctsnow[hvar < -0.5] = 100;
    pctsnow[(hvar >= -0.5) & (dvar < 0.5)] = hvar[(hvar >= -0.5) & (dvar < 0.5)] .* -100 + 50;
    return(pctsnow)

for y in wyoi: # for each year
    ppt_file = "/Volumes/LaCie/precip_cf/precip_wy%s.nc" % y
    wbt_file = "/Volumes/megadozer/CZO/wb_corrected/wb_wy%s.nc" % y
    # dpt_file = "/Volumes/megadozer/CZO/dp_corrected/dp_wy%s.nc" % y

    # get timesteps with precipitation
    p_ncid = nc.Dataset(ppt_file,'r')           # open ncdf file
    rd_var = p_ncid.variables['raw_data']       # pointer to ppt variable
    ts = np.ma.filled(rd_var[:],np.nan)         # bring in precip station data
    ts = np.nansum(ts,axis=1)>0                 # get times where there was precipitation
    ts = np.transpose(np.array(np.where(ts)))   # get indexes of times when there is precipitation
    tt = p_ncid.variables['time']               # get pointer to the times for that year
    time = nc.num2date(tt[:], tt.units)         # get times for this year in pandas Series
    time = time + np.timedelta64(7,'h')         # add 7 hour offset to get it in local time
    pind = pd.DataFrame(tt,index=time)          # pandas dataset for indexes and times
    del time
    del tt
    del ts
    del rd_var

    p_varid = p_ncid.variables['precipitation_amount']  # get precip var id
    
    if hflag == "w": # if using wet bulb temperature
        hfn = "/Volumes/megadozer/CZO/wb_corrected/wb_wy%s.nc"%y
        hnc = nc.Dataset(hfn,'r')
        hvn = hnc.variables["wet_bulb_temperature"]
    elif hflag == "d":
        hfn = "/Volumes/megadozer/CZO/dp_corrected/dp_wy%s.nc"%y
        hnc = nc.Dataset(hfn,'r')
        hvn = hnc.variables["dp"]


    for m in wym: 
        ts = (pind['date'].month==m) and (pind>0)   # index of times with precip
        pvar = p_varid[ts,:,:]
        hvar = hvn[ts,:,:]




# # preallocate memory
# monthly_mixed_mm_d = zeros(12,length(wyoi),'single'); # total mm
# monthly_mixed_pc_d = zeros(12,length(wyoi),'single'); # total percent
# monthly_snow_mm_d = zeros(12,length(wyoi),'single');  # total in mm
# monthly_snow_pc_d = zeros(12,length(wyoi),'single');  # total percent
# monthly_total_mm_d = zeros(12,length(wyoi),'single');  # total precip
# monthly_mixed_mm_w = zeros(12,length(wyoi),'single'); # total mm
# monthly_mixed_pc_w = zeros(12,length(wyoi),'single'); # total percent
# monthly_snow_mm_w = zeros(12,length(wyoi),'single');  # total in mm
# monthly_snow_pc_w = zeros(12,length(wyoi),'single');  # total percent
# monthly_total_mm_w = zeros(12,length(wyoi),'single');  # total precip

# for y = 1:length(wyoi) # for each water year
#     mixed_maps = zeros(rows,cols,length(wym),'single'); # mem for mixed precip maps.
#     snow_maps = zeros(rows,cols,length(wym),'single'); # mem for snow maps.
#     total_maps = zeros(rows,cols,length(wym),'single'); # mem for total precip maps.
        
#     for m = 1:length(wym)
#         if sum(find(mtime_ppt==wym(m))) > 0 # if there was precipitation that month
#             pvar = ppt_file.ppt_maps(:,:,find(mtime_ppt==wym(m)));
#             dvar = dpt_file.dpt_maps(:,:,find(mtime_ppt==wym(m)));
#             # calc # snow from dewpoint matrix from the following table
#             #      temperature                # snow     snow density
#             #                       T < -5 C   100#        75 kg/m^3
#             #               -5 C <= T < -3 C   100#       100 kg/m^3
#             #               -3 C <= T < -1.5 C 100#       150 kg/m^3
#             #             -1.5 C <= T < -0.5 C 100#       175 kg/m^3
#             #             -0.5 C <= T < 0 C     75#       200 kg/m^3
#             #                0 C <= T < 0.5 C   25#       250 kg/m^3
#             #              0.5 C <= T            0#         0 kg/m^3
#             pctsnow = zeros(size(dvar),'int8');
#             pctsnow(dvar <  -0.5) = 100;
#             pctsnow(dvar >= -0.5 & dvar < 0.5) = 50; #dvar(dvar >= -0.5 & dvar < 0.5) .* -100 + 50;
            
#             tt0 = zeros(size(pctsnow),'single'); # mem
#             tt0(pctsnow==50) = 1;       # id mixed events
#             tt1 = tt0 .* pvar;          # get mixed precip mass
#             mixed_maps(:,:,m) = sum(tt1,3);    # sum for month
#             monthly_mixed_mm(m,y) = sum(sum(mixed_maps(:,:,m).*msk))/numpix;
#             #  figure(1); iscet(mixed_maps(:,:,m)); title('mixed events') # plot
            
#             tt0 = zeros(size(pctsnow),'single');        # mem
#             tt0(pctsnow==100) = 1;             # id snow events
#             tt1 = tt0 .* pvar;                 # just keep snow precip
#             snow_maps(:,:,m) = sum(tt1,3);   # sum timesteps
#             monthly_snow_mm(m,y) = sum(sum(snow_maps(:,:,m).*msk))/numpix;
#             # figure(2); iscet(snow_maps(:,:,m)); title('snow events') # plot
#             clear pctsnow
            
#             total_maps(:,:,m) = sum(pvar,3);
#             monthly_total_mm(m,y) = sum(sum(total_maps(:,:,m).*msk))/numpix;
#             monthly_mixed_pc(m,y) = sum(sum(mixed_maps(:,:,m).*msk))/sum(sum(total_maps(:,:,m).*msk));
#             monthly_snow_pc(m,y) = sum(sum(snow_maps(:,:,m).*msk))/sum(sum(total_maps(:,:,m).*msk));
#             # figure(3); iscet(precip); title('total precip.')
#         else
#             mixed_maps(:,:,m) = zeros(size(msk));    # sum for month
#             monthly_mixed_mm(m,y) = 0;
#             snow_maps(:,:,m) = zeros(size(msk));   # sum timesteps
#             monthly_snow_mm(m,y) = 0;
#             total_maps(:,:,m) = zeros(size(msk));
#             monthly_total_mm(m,y) = 0;
#             monthly_mixed_pc(m,y) = 0;
#             monthly_snow_pc(m,y) = 0;
#         end
#         disp(['done with ' num2str(wym(m),'#02d') '-' num2str(wyoi(y))])
        
#     end
#     save([sav_dir 'total_maps_' num2str(wyoi(y)) '.mat'],'total_maps','-v7.3');
#     save([sav_dir 'snow_maps_' num2str(wyoi(y)) '.mat'],'snow_maps','-v7.3');
#     save([sav_dir 'mixed_maps_' num2str(wyoi(y)) '.mat'],'mixed_maps','-v7.3');
#     clear *maps*
# end
# disp('done... saving data')
# # save data
# save([sav_dir 'monthly_summary_' num2str(wyoi(1)) '_' num2str(wyoi(end)) '.mat'],...
#     'monthly_mixed_mm','monthly_mixed_pc','monthly_snow_mm','monthly_snow_pc',...
#     'monthly_total_mm','-v7.3');
    

# ## 
# figure(4); plot(1:12,[monthly_mixed(:,1) monthly_snow(:,1) monthly_total],'o-','Linewidth',2)
# set(gca,'XTick',1:12,'XTickLabel',{'Oct' 'Nov' 'Dec' 'Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep'});
# set(gca,'FontSize',16)
# legend('Mixed','Snow','Total')


