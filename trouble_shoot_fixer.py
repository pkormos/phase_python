'''
Created on Nov 18, 2016

@author: pkormos
'''
import netCDF4 as nc

# rename rh_wyxxxx.nc back to dk_out_xxxx.nc (command line mv)
# rename variable "relative_humidity" back to "variable" 
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/dk_out_1984.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r+')            # open dataset
rn.renameVariable('relative_humidity','variable')   # rename variable
rn.close()
# run dk command with new config and input files e.g. dk -t 5 -k ./dk_config_wy1984_0_patch.txt
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/dk_out_1984.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r+')            # open dataset
rn.renameVariable('variable','relative_humidity')   # rename variable
rn.close()
  
