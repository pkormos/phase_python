'''
Created on Oct 31, 2016

script to plot and correct relative humidity data based on the plots from 
humidity_analysis.py. 

@author: pkormos
'''

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
from pandas import DataFrame, date_range

wyoi = np.arange(1984, 2003)  # water years of interest

# bring in precipitation data
p_list = np.array(['rc-047', 'rc-055', 'rc-059', 'rc-074', 'rc-075', 'rc-076', 'rc-078', 'rc-083', # manually bring in column headers
    'rc-088', 'rc-095b', 'rc-097', 'rc-106', 'rc-114', 'rc-119', 'rc-128', 'rc.fl-057',
    'rc.lsc-127', 'rc.mc-043041', 'rc.mc-053', 'rc.mc-054', 'rc.mc-061', 'rc.mc-072',
    'rc.ng-098c', 'rc.ng-108', 'rc.sc-012', 'rc.sc-015', 'rc.sc-023', 'rc.sc-024',
    'rc.sc-031','rc.sc-045', 'rc.sc.mp-033', 'rc.sum-049', 'rc.tg-116c','rc.tg-126',
    'rc.tg-145', 'rc.tg-147', 'rc.tg-155', 'rc.tg-156', 'rc.tg-165', 'rc.tg-167',
    'rc.tg-174_ppt', 'rc.tg.dc-144', 'rc.tg.dc-154', 'rc.tg.dc-163', 'rc.tg.dc-163',
    'rc.tg.dc.jd-124', 'rc.tg.dc.jd-124b', 'rc.tg.dc.jd-125', 'rc.tg.rme-166b',
    'rc.tg.rme-176', 'rc.tg.rme-rmsp', 'rc.tg.rmw-166x94','rc.usc-138031', 
    'rc.usc-138044', 'rc.usc-d03','rc.usc-j10', 'rc.usc-l21'])
pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
pn = nc.Dataset(pnc, 'r')            # create precip ncdf file
pvar = pn.variables["precipitation"]
ppt = DataFrame(pvar[:], index=date_range('1962-1-1', '2014-12-31 23:00', freq='H'), columns = p_list) # make pandas dataset
pn.close()

# bring in relative humidity data
r_list = np.array(['rc-076','rc-095b','rc-128','rc.lsc-127','rc.sc-012','rc.sc-031',
    'rc.tg-145','rc.tg-167','rc.tg-167b','rc.tg-174','rc.tg.dc-144',
    'rc.tg.dc-163',
    'rc.tg.dc.jd-124',
    'rc.tg.dc.jd-124b',
    'rc.tg.dc.jd-125',
    'rc.tg.rme-166b',
    'rc.tg.rme-176',
    'rc.tg.rme-rmsp',
    'rc.usc-d03',
    'rc.usc-j10',
    'rc.usc-l21',
    'rc-076_tf',
    'rc-128_tf',
    'rc.lsc-127_tf',
    'rc.tg.rme-176_tf'])
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_rh_pk.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rh = DataFrame(rvar[:], index=date_range('18-Jun-1981 11:00:00', '01-Oct-2014', freq='H'), columns = r_list) # make pandas dataset
rn.close()

# plot wy1986 076 as test code for figuring out what and where to correct this rh data
# looks like we need to add 0.10 to the water year... but when to start? goes through at
# least 1991.
# wy0  = 1986
stn0 = 'rc-076'

# # plot histograms for rh during precip events
# fig1 = plt.figure(num=1, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh.ix['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y,[stn0]] # get rh time slice for stn0
#     ppt0 = ppt.ix['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y,[stn0]] # get rh time slice for stn0]
#     pind = ppt0>0      # index to times with precip
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[pind].dropna()
#     whst = np.histogram(rh0[pind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig1.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
#     plt.show()
# fig1.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_076_before.tif', dpi=100) 


# plot original and corrected data
stretcher1=0.84
stretcher2=0.88
fig2 = plt.figure(num=2, figsize=(14,8.5), dpi=100, facecolor='w')
ax2 = fig2.add_axes([.05, .05, .9, .9]) 
rh.ix['10/1/1982 0:00':'9/30/1999 23:00',[stn0]].plot(style = 'b-', ax=ax2) # plot time series before modification
ax2.hlines(y=stretcher1,xmin='9/22/1985 17:00', xmax='6/1/1992 12:00',color='r', linewidth=2) # plot line showing area to be changed
ax2.hlines(y=stretcher2,xmin='6/1/1992 13:00', xmax='10/26/1995 0:00',color='r', linewidth=2) # plot line showing area to be changed
xmin, xmax = ax2.get_xlim()
ax2.hlines(y=.13, xmin=xmin, xmax=xmax-1, color='r', linewidth=2)

# apply stretch correction
rh.ix['9/22/1985 17:00':'6/1/1992 12:00',[stn0]] = rh.ix['9/22/1985 17:00':'6/1/1992 12:00',[stn0]]/stretcher1
rh.ix['9/22/1985 17:00':'6/1/1992 12:00',[stn0]] = rh.ix['9/22/1985 17:00':'6/1/1992 12:00',[stn0]].clip_upper(1)

rh.ix['6/1/1992 13:00':'10/26/1995 0:00',[stn0]] = rh.ix['6/1/1992 13:00':'10/26/1995 0:00',[stn0]]/stretcher2
rh.ix['6/1/1992 13:00':'10/26/1995 0:00',[stn0]] = rh.ix['6/1/1992 13:00':'10/26/1995 0:00',[stn0]].clip_upper(1)

rh.ix['9/22/1985 17:00':'6/1/1992 12:00',[stn0]].plot(style = 'g-', ax=ax2) # plot time series after mod. green
rh.ix['6/1/1992 13:00':'10/26/1995 0:00',[stn0]].plot(style = 'k-', ax=ax2) # plot time series after mod. green

# # this method made the minimums too high compared to wy 1984-5 and wy96-99
# rh.ix['9/22/1985 17:00':'9/30/1995 23:00',[stn0]] = rh.ix['9/22/1985 17:00':'9/30/1995 23:00',[stn0]]+0.15 
# rh.ix['9/22/1985 17:00':'9/30/1995 23:00',[stn0]] = rh.ix['9/22/1985 17:00':'9/30/1995 23:00',[stn0]].clip_upper(1)

ax12 = ax2.twinx()
ppt.ix['10/1/1982 0:00':'9/30/1999 23:00',[stn0]].plot(style= 'g-', ax=ax12)

fig2.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_076_before_after.tif', dpi=100) 


fig3 = plt.figure(num=3, figsize=(8.5,14), dpi=100, facecolor='w')
for y in np.nditer(wyoi[0:14]):    
    rh0  = rh.ix['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y,[stn0]] # get rh time slice for stn0
    ppt0 = ppt.ix['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y,[stn0]] # get rh time slice for stn0]
    pind = ppt0>0      # index to times with precip
    b_edg = np.linspace(0, 1, num=51)       # bin edges
    width = 0.02
    wts = ppt0[pind].dropna()
    whst = np.histogram(rh0[pind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
    ax3 = fig3.add_subplot(7,2,y-1983)
    rects1 = ax3.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
    plt.axis([0,1.01,0,plt.ylim()[1]])
    plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
fig3.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_076_after89.tif', dpi=100) 












