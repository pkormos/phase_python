'''
Created on Nov 2, 2016

script to plot and correct relative humidity data from 127 based on the plots from 
humidity_analysis.py. and time series plot. updates netcdf file with station data.

@author: pkormos
'''

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series, date_range

def rhstretch(data,old_low,new_low,old_high):
    slp = (1-new_low)/(old_high-old_low)
    itc = new_low - old_low * slp
    return(data*slp+itc)

stn0 = 'rc.lsc-127'
wyoi = np.arange(1984, 2015)  # water years of interest

# bring in precipitation data
pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
pn = nc.Dataset(pnc, 'r')            # create precip ncdf file
pvar = pn.variables["precipitation"]
p_st = pn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(pvar)[1])]
p_list = [p_st.getncattr(tt_list[k]) for k in range(np.shape(pvar)[1])] # sorted rh station list
ppt = Series(np.ma.filled(pvar[:,p_list.index(stn0)],np.nan).ravel(),index=date_range('1962-1-1', '2014-12-31 23:00', freq='H')) # make pandas dataset
pn.close()

# bring in relative humidity data
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_rh_pk.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rh_st = rn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(rvar)[1])]
r_list = [rh_st.getncattr(tt_list[k]) for k in range(np.shape(rvar)[1])] # sorted rh station list
rh = Series(np.ma.filled(rvar[:,r_list.index(stn0)],np.nan).ravel(),index=date_range('18-Jun-1981 11:00:00', '01-Oct-2014', freq='H')) # make pandas dataset
rn.close()

# # plot up data to check it
# fig0 = plt.figure(num=10, figsize=(11,8.5))
# fig0.add_subplot(2,1,1)
# ppt.plot()
# fig0.add_subplot(2,1,2)
# rh.plot()

# code to correct 076 from 9/22/1985 17:00 to 10/26/1995 0:00

## plot histograms for rh during precip events
# fig1 = plt.figure(num=1, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig1.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
# #     plt.show()
# fig1.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_127_before.tif', dpi=100) 

# from these histograms, it looks like WY1993 and forward is good. 
# WY1985 may have started out good but then 85 and 86 have similar patterns
# WY87, 88, and 90 also look like they have the same pattern
# WY86 and 89? also. start plotting and see

## plot original and corrected data
# plot original data
# fig2 = plt.figure(num=2, figsize=(14,8.5), dpi=100, facecolor='w')
# ax2 = fig2.add_axes([.05, .05, .9, .9]) 
# rh['10/1/1982 0:00':'9/30/1999 23:00'].plot(style = 'b-', ax=ax2) # plot time series before modification

## correct first section of data
stretcher1=0.87     # what the upper rh does go up to
stretcher1b=0.07    # what the lower rh should drop to 
stretcher1bb = 0.16 # what the lower rh does drop to
# l1 = ax2.hlines(y=stretcher1, xmin='12/5/1984 15:00', xmax='3/23/1987 0:00',color='r', linewidth=2) # plot line showing area to be changed
# l2 = ax2.hlines(y=stretcher1b,xmin='12/5/1984 15:00', xmax='9/30/1999 23:00',color='r', linewidth=2) # plot line showing area to be changed
# ax2.hlines(y=1,xmin='12/5/1984 15:00', xmax='9/30/1999 23:00',color='k')
# l3 = ax2.vlines(ymin=0,ymax=1,x='3/23/1987 0:00',color='r',linewidth=2)
# l4 = ax2.hlines(y=stretcher1bb,xmin='12/5/1984 15:00', xmax='3/23/1987 0:00',color='r', linewidth=2) # plot line showing area to be changed

# correct first section and plot it
rh['12/5/1984 15:00':'3/23/1987 0:00'] = rhstretch(rh['12/5/1984 15:00':'3/23/1987 0:00'],stretcher1bb,stretcher1b,stretcher1)
rh['12/5/1984 15:00':'3/23/1987 0:00'] = rh['12/5/1984 15:00':'3/23/1987 0:00'].clip_upper(1)
# l5 = rh['12/5/1984 15:00':'3/23/1987 0:00'].plot(style='g-',ax=ax2)

## correct second section of data
str2   = 0.85
str2b  = 0.07    # what the lower rh should drop to
str2bb = 0.08
# l1 = ax2.hlines(y=str2,xmin = '3/23/1987 1:00',xmax='8/1/1988 10:00',color='r',linewidth=2)
# l2 = ax2.hlines(y=str2bb,xmin = '3/23/1987 1:00',xmax='8/1/1988 10:00',color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x='8/1/1988 10:00',color='r',linewidth=2)

# correct second section and plot it
rh['3/23/1987 1:00':'8/1/1988 10:00'] = rhstretch(rh['3/23/1987 1:00':'8/1/1988 10:00'],str2bb,str2b,str2)
rh['3/23/1987 1:00':'8/1/1988 10:00'] = rh['3/23/1987 1:00':'8/1/1988 10:00'].clip_upper(1)
# l6 = rh['3/23/1987 1:00':'8/1/1988 10:00'].plot(style='k-',ax=ax2)

## correct third section of data
st3='1988-10-18 15:00'
nd3='1989-07-29 12:00'
str3   = 0.92
str3b  = 0.07
str3bb = 0.07
# l1 = ax2.hlines(y=str3,xmin=st3,xmax=nd3,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=st3,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd3,color='r',linewidth=2)

# correct third section of data and plot it
rh[st3:nd3] = rhstretch(rh[st3:nd3],str3bb,str3b,str3)
rh[st3:nd3] = rh[st3:nd3].clip_upper(1)
# l4 = rh[st3:nd3].plot(style='c-',ax=ax2)

## correct fourth section of data
st4='1989-07-29 13:00'
nd4='1990-03-14 23:00'
str4   = 0.85
str4b  = 0.07
str4bb = 0.07
# l1 = ax2.hlines(y=str4,xmin=st4,xmax=nd4,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=nd4,color='r',linewidth=2)

# correct fourth section of data and plot it
rh[st4:nd4] = rhstretch(rh[st4:nd4],str4bb,str4b,str4)
rh[st4:nd4] = rh[st4:nd4].clip_upper(1)
# l3 = rh[st4:nd4].plot(style='m-',ax=ax2)

## correct fifth section of data
st5='1990-11-14 06:00'
nd5='1991-07-26 16:00'
str5   = 0.96
str5b  = 0.07
str5bb = 0.07
# l1 = ax2.hlines(y=str5,xmin=st5,xmax=nd5,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=st5,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd5,color='r',linewidth=2)

# correct fifth section of data and plot it
rh[st5:nd5] = rhstretch(rh[st5:nd5],str5bb,str5b,str5)
rh[st5:nd5] = rh[st5:nd5].clip_upper(1)
# l4 = rh[st5:nd5].plot(style='k-',ax=ax2)

## correct sixth section of data
st6='1991-07-26 17:00'
nd6='1992-11-03 12:00'
str6   = 0.98
str6b  = 0.07
str6bb = 0.07
# l1 = ax2.hlines(y=str6,xmin=st6,xmax=nd6,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=nd6,color='r',linewidth=2)

# correct sixth section of data and plot it
rh[st6:nd6] = rhstretch(rh[st6:nd6],str6bb,str6b,str6)
rh[st6:nd6] = rh[st6:nd6].clip_upper(1)
# l3 = rh[st6:nd6].plot(style='g-',ax=ax2)

## correct seventh section of data
st7='1995-02-09 12:00'
nd7='1996-06-28 09:00'
str7   = 0.98
str7b  = 0.07
str7bb = 0.07
# l1 = ax2.hlines(y=str7,xmin=st7,xmax=nd7,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=st7,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd7,color='r',linewidth=2)

# correct seventh section of data and plot it
rh[st7:nd7] = rhstretch(rh[st7:nd7],str7bb,str7b,str7)
rh[st7:nd7] = rh[st7:nd7].clip_upper(1)
# l4 = rh[st7:nd7].plot(style='y-',ax=ax2)

# ax12 = ax2.twinx()
# ppt['10/1/1982 0:00':'9/30/1999 23:00'].plot(style= 'g-', ax=ax12)
# 
# fig2.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_127_before_after.tif', dpi=100) 
# 
# fig3 = plt.figure(num=3, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig3.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
#     #     plt.show()
# fig3.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_127_after.tif', dpi=100) 


# ################################################################################################################
# ## plot original and corrected data 2003-2014
# ################################################################################################################
# # plot histograms for rh 2000 -> 2014 during precip events
# fig4 = plt.figure(num=4, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[17:31]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     pind = ppt0>0      # index to times with precip
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[pind].dropna()
#     whst = np.histogram(rh0[pind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig4.add_subplot(7,2,y-2000)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
# #     plt.show()
# fig4.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_127_before_2001_2014.tif', dpi=100) 


# # time series plot and corrections
# fig5 = plt.figure(num=5, figsize=(14,8.5), dpi=100, facecolor='w')
# ax15 = fig5.add_axes([.05, .05, .9, .9]) 
# ppt['10/1/1999 0:00':'9/30/2014 23:00'].plot(style= 'g-', ax=ax15)
# ax5 = ax15.twinx()
# rh['10/1/1999 0:00':'9/30/2014 23:00'].plot(style = 'b-', ax=ax5) # plot time series before modification
# plt.legend(loc='center', bbox_to_anchor=(1, 0.5))

## correct first section of data WY05-WY14
stt='2004-10-01 01:00'
ndt='2007-09-01 00:00'
strt  = 0.97 # what the upper rh does go up to
strb  = 0.00 # what the lower rh should drop to 
strbb = 0.00 # what the lower rh does drop to
# l1 = ax5.hlines(y=strt,xmin=stt, xmax=ndt,color='r', linewidth=2) # plot line showing area to be changed

# correct first section of data and plot it 05-14
rh[stt:ndt] = rhstretch(rh[stt:ndt],strbb,strb,strt)
rh[stt:ndt] = rh[stt:ndt].clip_upper(1)
# l2 = rh[stt:ndt].plot(style='c-',ax=ax5)

## correct 2nd section of data WY05-WY14
stt='2011-10-01 01:00'
ndt='2012-10-01 00:00'
strt  = 0.99 # what the upper rh does go up to
strb  = 0.00 # what the lower rh should drop to 
strbb = 0.00 # what the lower rh does drop to
# l1 = ax5.hlines(y=strt,xmin=stt, xmax=ndt,color='r', linewidth=2) # plot line showing area to be changed
rh[stt:ndt] = rhstretch(rh[stt:ndt],strbb,strb,strt)
rh[stt:ndt] = rh[stt:ndt].clip_upper(1)
# l2 = rh[stt:ndt].plot(style='m-',ax=ax5)

## correct 3rd section of data WY05-WY14
stt='2013-10-01 01:00'
ndt='2014-10-01 00:00'
strt  = 0.97 # what the upper rh does go up to
strb  = 0.00 # what the lower rh should drop to 
strbb = 0.00 # what the lower rh does drop to
# l1 = ax5.hlines(y=strt,xmin=stt, xmax=ndt,color='r', linewidth=2) # plot line showing area to be changed
rh[stt:ndt] = rhstretch(rh[stt:ndt],strbb,strb,strt)
rh[stt:ndt] = rh[stt:ndt].clip_upper(1)
# l2 = rh[stt:ndt].plot(style='k-',ax=ax5)

# fig2.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_127_before_after2015.tif', dpi=100) 


# fig6 = plt.figure(num=6, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[17:31]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     pind = ppt0>0      # index to times with precip
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[pind].dropna()
#     whst = np.histogram(rh0[pind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig6.add_subplot(7,2,y-2000)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
# #     plt.show()
# 
# fig6.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_127_after_2001_2014.tif', dpi=100) 


## update the netcdf file with the corrected data
rn = nc.Dataset(rnc, 'r+')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rvar[:,r_list.index(stn0)] = np.array(rh)
 
# a = rvar[:,r_list.index(stn0)]
# plt.plot(a)
rn.close()



