'''
Created on Nov 3, 2016
script to plot and correct relative humidity data from usc j10 based on the plots from 
humidity_analysis.py. 

@author: pkormos
'''

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series, date_range

def rhstretch(data,old_low,new_low,old_high):
    slp = (1-new_low)/(old_high-old_low)
    itc = new_low - old_low * slp
    return(data*slp+itc)

stn0 = 'rc.usc-j10'
wyoi = np.arange(1984, 2015)  # water years of interest

# bring in precipitation data
pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
pn = nc.Dataset(pnc, 'r')            # create precip ncdf file
pvar = pn.variables["precipitation"]
p_st = pn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(pvar)[1])]
p_list = [p_st.getncattr(tt_list[k]) for k in range(np.shape(pvar)[1])] # sorted rh station list
ppt = Series(np.ma.filled(pvar[:,p_list.index(stn0)],np.nan).ravel(),index=date_range('1962-1-1', '2014-12-31 23:00', freq='H')) # make pandas dataset
pptd3 = Series(np.ma.filled(pvar[:,p_list.index('rc.usc-d03')],np.nan).ravel(),index=date_range('1962-1-1', '2014-12-31 23:00', freq='H')) # make pandas dataset
pn.close()

# # bring in relative humidity data
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_rh_pk.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rh_st = rn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(rvar)[1])]
r_list = [rh_st.getncattr(tt_list[k]) for k in range(np.shape(rvar)[1])] # sorted rh station list
rh = Series(np.ma.filled(rvar[:,r_list.index(stn0)],np.nan).ravel(),index=date_range('18-Jun-1981 11:00:00', '01-Oct-2014', freq='H')) # make pandas dataset
rn.close()

# # plot up data to check it
# fig0 = plt.figure(num=10, figsize=(17,8.5))
# ax1 = fig0.add_subplot(3,1,1)
# ax1.set_xlim('1983/10/1','2014/10/1')
# ppt.plot()
# ax2 = fig0.add_subplot(3,1,2)
# ax2.set_xlim('1983/10/1','2014/10/1')
# pptd3.plot()
# ax3 = fig0.add_subplot(3,1,3)
# ax3.set_xlim('1983/10/1','2014/10/1')
# rh.plot()
# ax3.hlines(y=1,xmin='1983/10/1',xmax='2014/10/1')

# # plot histograms for j10 rh during precip events
# fig1 = plt.figure(num=1, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0d = pptd3['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0[ppt0.isnull()] = ppt0d[ppt0.isnull()]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig1.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
#     plt.show()
# fig1.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_j10_before.tif', dpi=100) 

## plot original and corrected data
# # plot original data
# fig2 = plt.figure(num=2, figsize=(14,8.5), dpi=100, facecolor='w')
# ax12 = fig2.add_axes([.05, .05, .9, .9]) 
# ppt['10/1/1982 0:00':'9/30/2006 23:00'].plot(style= 'g-', ax=ax12)
# 
# ax2 = ax12.twinx()
# rh['10/1/1982 0:00':'9/30/2006 23:00'].plot(style = 'b-', ax=ax2) # plot time series before modification

## correct first section of data
st1='1983-10-1 0:00'
nd1='1984-9-20 3:00'
stretcher1=0.95     # what the upper rh does go up to
strb=0.1     # what the lower rh should drop to 
stretcher1bb = 0.21 # what the lower rh does drop to
# l0 = ax2.hlines(y=1,xmin=st1,xmax = '9/30/2006 23:00',color='k')
# l1 = ax2.hlines(y=strb,xmin=st1,xmax='9/30/1999 23:00',color='r', linewidth=2) # plot line showing approx low rh for time series
# l2 = ax2.hlines(y=stretcher1, xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l3 = ax2.hlines(y=stretcher1bb,xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l4 = ax2.vlines(ymin=0,ymax=1,x=st1,color='r',linewidth=2)
# l5 = ax2.vlines(ymin=0,ymax=1,x=nd1,color='r',linewidth=2)

# correct first section and plot it
rh[st1:nd1] = rhstretch(rh[st1:nd1],stretcher1bb,strb,stretcher1)
rh[st1:nd1] = rh[st1:nd1].clip_upper(1)
# l5 = rh[st1:nd1].plot(style='g-',ax=ax2)

## correct second section of data
st2='1984-09-20 4:00'
nd2='1985-10-01 0:00'
str2   = 0.91
str2bb = 0.14
# l1 = ax2.hlines(y=str2,xmin = st2,xmax=nd2,color='r',linewidth=2)
# l2 = ax2.hlines(y=str2bb,xmin = st2,xmax=nd2,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd2,color='r',linewidth=2)

# correct second section and plot it
rh[st2:nd2] = rhstretch(rh[st2:nd2],str2bb,strb,str2)
rh[st2:nd2] = rh[st2:nd2].clip_upper(1)
# l4 = rh[st2:nd2].plot(style='k-',ax=ax2)

## correct third section of data
st3='1985-10-01 01:00'
nd3='1986-08-01 00:00'
str3   = 0.92
str3bb = 0.12
# l1 = ax2.hlines(y=str3,xmin=st3,xmax=nd3,color='r',linewidth=2)
# l2 = ax2.hlines(y=str3bb,xmin=st3,xmax=nd3,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd3,color='r',linewidth=2)
 
# correct third section of data and plot it
rh[st3:nd3] = rhstretch(rh[st3:nd3],str3bb,strb,str3)
rh[st3:nd3] = rh[st3:nd3].clip_upper(1)
# l4 = rh[st3:nd3].plot(style='c-',ax=ax2)
 
## correct fourth section of data
st4='1986-08-01 01:00'
nd4='1986-11-03 00:00'
str4   = 0.9
str4bb = 0.12
# l1 = ax2.hlines(y=str4,xmin=st4,xmax=nd4,color='r',linewidth=2)
# l2 = ax2.hlines(y=str4bb,xmin=st4,xmax=nd4,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd4,color='r',linewidth=2)
 
# correct fourth section of data and plot it
rh[st4:nd4] = rhstretch(rh[st4:nd4],str4bb,strb,str4)
rh[st4:nd4] = rh[st4:nd4].clip_upper(1)
# l3 = rh[st4:nd4].plot(style='m-',ax=ax2)
 
## correct fifth section of data
st5='1986-11-03 01:00'
nd5='1987-10-01 00:00'
str5   = 0.91
str5bb = 0.15
# l1 = ax2.hlines(y=str5,xmin=st5,xmax=nd5,color='r',linewidth=2)
# l2 = ax2.hlines(y=str5bb,xmin=st5,xmax=nd5,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd5,color='r',linewidth=2)
 
# correct fifth section of data and plot it
rh[st5:nd5] = rhstretch(rh[st5:nd5],str5bb,strb,str5)
rh[st5:nd5] = rh[st5:nd5].clip_upper(1)
# l4 = rh[st5:nd5].plot(style='k-',ax=ax2)
 
## correct sixth section of data
st6='1987-10-01 01:00'
nd6='1988-10-01 00:00'
str6   = 0.89
str6bb = 0.15
# l1 = ax2.hlines(y=str6,xmin=st6,xmax=nd6,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=nd6,color='r',linewidth=2)
 
# correct sixth section of data and plot it
rh[st6:nd6] = rhstretch(rh[st6:nd6],str6bb,strb,str6)
rh[st6:nd6] = rh[st6:nd6].clip_upper(1)
# l3 = rh[st6:nd6].plot(style='g-',ax=ax2)
 
## correct seventh section of data
st7='1988-10-01 01:00'
nd7='1989-04-05 13:00'
str7   = 0.97
str7bb = 0.1
# l1 = ax2.hlines(y=str7,xmin=st7,xmax=nd7,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=nd7,color='r',linewidth=2)
 
# correct seventh section of data and plot it
rh[st7:nd7] = rhstretch(rh[st7:nd7],str7bb,strb,str7)
rh[st7:nd7] = rh[st7:nd7].clip_upper(1)
# l4 = rh[st7:nd7].plot(style='y-',ax=ax2)

## correct eighth section of data
st8='1989-04-05 14:00'
nd8='1989-10-01 00:00'
str8   = 0.87
str8bb = 0.1
# l1 = ax2.hlines(y=str8,xmin=st8,xmax=nd8,color='r',linewidth=2)
# l2 = ax2.vlines(ymin=0,ymax=1,x=nd8,color='r',linewidth=2)
 
# correct seventh section of data and plot it
rh[st8:nd8] = rhstretch(rh[st8:nd8],str8bb,strb,str8)
rh[st8:nd8] = rh[st8:nd8].clip_upper(1)
# l4 = rh[st8:nd8].plot(style='y-',ax=ax2)

## correct ninth section of data
st9='1990-03-16 06:00'
nd9='1990-08-16 00:00'
str9   = 0.87
str9bb = 0.27
# l1 = ax2.hlines(y=str9,xmin=st9,xmax=nd9,color='r',linewidth=2)
# l2 = ax2.hlines(y=str9bb,xmin=st9,xmax=nd9,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=st9,color='r',linewidth=2)
# l4 = ax2.vlines(ymin=0,ymax=1,x=nd9,color='r',linewidth=2)
 
# correct seventh section of data and plot it
rh[st9:nd9] = rhstretch(rh[st9:nd9],str9bb,strb,str9)
rh[st9:nd9] = rh[st9:nd9].clip_upper(1)
# l4 = rh[st9:nd9].plot(style='y-',ax=ax2)

## correct tenth section of data
st0='2003-10-01 00:00'
nd0='2005-10-01 00:00'
str0   = 0.98
str0bb = 0.1
# l1 = ax2.hlines(y=str0,xmin=st0,xmax=nd0,color='r',linewidth=2)
# l2 = ax2.hlines(y=str0bb,xmin=st0,xmax=nd0,color='r',linewidth=2)
# l3 = ax2.vlines(ymin=0,ymax=1,x=st0,color='r',linewidth=2)
# l4 = ax2.vlines(ymin=0,ymax=1,x=nd0,color='r',linewidth=2)
 
# correct seventh section of data and plot it
rh[st0:nd0] = rhstretch(rh[st0:nd0],str0bb,strb,str0)
rh[st0:nd0] = rh[st0:nd0].clip_upper(1)
# l4 = rh[st0:nd0].plot(style='y-',ax=ax2)


# fig2.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_j10_before_after.tif', dpi=100) 
# 
# fig3 = plt.figure(num=3, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0d = pptd3['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0[ppt0.isnull()] = ppt0d[ppt0.isnull()]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig3.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
#     #     plt.show()
# fig3.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_j10_after.tif', dpi=100) 

# fig4 = plt.figure(num=4, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[14:28]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0d = pptd3['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0[ppt0.isnull()] = ppt0d[ppt0.isnull()]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig4.add_subplot(7,2,y-1997)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
# #     plt.show()

rh = rh.clip_upper(1) # clip all of the values greater than 1

## update the netcdf file with the corrected data
rn = nc.Dataset(rnc, 'r+')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rvar[:,r_list.index(stn0)] = np.array(rh)
 
# a = rvar[:,r_list.index(stn0)]
# plt.plot(a)
rn.close()







