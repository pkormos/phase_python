'''
Created on June 6, 2016
This code will calculate wetbulb temperature from air temp. and dewpoint temp. to make 
phase information for each timestep that there is precipitation. Phase info will be
in the form of percent snow.

MODIFIED 7/11/2016 --- modified to store wetbulb temperature images to obtain trends 
MODIFIED 11/25/2016 -- modified significantly to remake wet bulb temperature grids and 
	summarize results after in depth correction qa qc of relative humidity data 1984 - 1993
@author: pkormos
'''

import os
import netCDF4 as nc
import numpy as np
import progressbar
import subprocess as sp 
from isnobal import ipw
# import matplotlib.pyplot as plt

IPW = "/Users/pkormos/ipw/bin"
PATH = os.environ.copy()["PATH"] + ':/usr/local/bin:' + IPW
tmp_dir = '/Users/pkormos/scratch/'

# location of IPW mask and bring in dem
dem = np.genfromtxt('/Users/pkormos/rcew_met_dist/terrain/rcew_10m.txt', delimiter=' ',skip_header=6)
msk_file = '/Users/pkormos/rcew_met_dist/terrain/msk010.ipw'

# ipw geohdr information ########################
u = 4795634.73
v = 511405.27
du = -10
dv = 10
units = 'meters'
csys = 'UTM'
nbits = 16
#################################################

## loop through water years
wyoi = np.arange(1997,1998)
for y in wyoi:
    print('begin ' + str(y))
    
    # define netcdf file names to access and write
    dp_ncdfFile = '/Volumes/megadozer/CZO/dp_corrected/dp_wy%s.nc'%y
    ta_ncdfFile = '/Volumes/SHARE1/Mac_Files/team-files/Patrick/ta_not_rounded/ta_wy%s.nc'%y
    wbn_ncdfFile = '/Volumes/megadozer/CZO/wb_corrected/wb_wy%s.nc'%y
    
    # open netcdf files for reading and writing
    dpn = nc.Dataset(dp_ncdfFile, 'r') # open dew point ncdf file for reading
    tan = nc.Dataset(ta_ncdfFile, 'r') # open air temperature ncdf file for reading
    wbn = nc.Dataset(wbn_ncdfFile,'w') # create wet bulb temperature ncdf file
        
    ## work on phase netcdf files
    # copy dimensions over from dp to phase netcdf files
    tdim = dpn.dimensions['time']
    ydim = dpn.dimensions['y']
    xdim = dpn.dimensions['x']
    
    wbn.createDimension(tdim.name,0)
    wbn.createDimension(ydim.name,len(ydim))
    wbn.createDimension(xdim.name,len(xdim))
    
    # copy time variable over
    tvar = dpn.variables['time']
    tvar_wbn = wbn.createVariable(tvar.name,tvar.datatype,tvar.dimensions) # make same tvar in snow
    tvar_wbn[:] = tvar[:] # add data to variable
    
    # copy y variable over
    yvar = dpn.variables['y'] # get yvar
    yvar_wbn = wbn.createVariable(yvar.name,yvar.datatype,yvar.dimensions) # put that yvar in snow
    yvar_wbn[:] = yvar[:] # put y coords in there
    
    # copy x variable over
    xvar = dpn.variables['x'] # get xvar
    xvar_wbn = wbn.createVariable(xvar.name,xvar.datatype,xvar.dimensions) # make same xvar in snow
    xvar_wbn[:] = xvar[:] # put x coords in there
    
    # Copy variable attributes
    tvar_wbn.setncatts({k: tvar.getncattr(k) for k in tvar.ncattrs()})
    yvar_wbn.setncatts({k: yvar.getncattr(k) for k in yvar.ncattrs()})
    xvar_wbn.setncatts({k: xvar.getncattr(k) for k in xvar.ncattrs()})
     
    # create the wet bulb variable 
    wbn_var = wbn.createVariable('wet_bulb_temperature','f4',('time','y','x'),fill_value=-9999) # create pct snow variable
    wbn_var.units = 'degree_celcius'
    wbn_var.long_name = 'wet bulb temperature in degree C'
    wbn_var.grid_mapping_name = 'universal_transverse_mercator'
    wbn_var.coordinates = 'lat lon'
    
    timesteps = range(0,len(tvar[:]))                # timesteps to loop over
    # timesteps = np.arange(8700,8761)                # timesteps to loop over
    pbar = progressbar.ProgressBar(len(timesteps)).start()
    for ts in timesteps:                             # for ts in timeStep:
        i = ipw.IPW()                            # create ipw space to be used for input to iwbt (wet bulb temp function)
        i.new_band(dem)                          # put dem in band 0
        ta = tan.variables['ta'][ts,:,:]         # pull out air temp from netcdf file
        i.new_band(ta)                           # put ta in band 1
        dp = dpn.variables['dp'][ts,:,:]         # pull out dp
        i.new_band(dp)                           # put dp in band 2
        i.add_geo_hdr([u, v], [du, dv], units, csys) # add geo header
        i.write('%s%s' % (tmp_dir,  'iwbt_in.ipw'), nbits) # write the file
        cmd = 'iwbt -m ' + msk_file + ' < ' + tmp_dir + 'iwbt_in.ipw  > ' + tmp_dir + 'wb.ipw' # build iwbt command 
        proc = sp.Popen(cmd, shell=True, env={"PATH": PATH,"IPW": IPW}).wait() # run iwbt
        i = ipw.IPW(tmp_dir + 'wb.ipw') # read wet bulb temp. image
        wbn_var[ts,:,:] = i.bands[0].data # write the wetbulb temp
        if np.mod(ts,100) == 0:
            wbn.sync()
            pbar.update(ts)
    pbar.finish()
    tan.close()
    wbn.close()
    dpn.close()
# plt.imshow(ppt)
# plt.colorbar()


    

