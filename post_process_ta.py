'''
Created on Nov 21, 2016
Script to post process 1985 not rounded air temperature on the SHARE drive. it was missing for some reason
rebuilt it from the config and input files located there.
@author: pkormos
'''

import netCDF4 as nc
import numpy as np
from pandas import DataFrame, offsets
import os
import subprocess as sp 

wyoi = np.arange(1985, 1986)  # water years of interest
outdir = '/Volumes/SHARE1/Mac_Files/team-files/Patrick/ta_not_rounded/'
os.chdir(outdir) # goto directory

## loop through water years to post process net cdf files from dk
for y in wyoi: # for each year
        
    ncname = "ta_wy%s.nc" % y           # define netcdf file name
    # os.rename("dk_out_%s.nc"%y, ncname) # rename netcdf file
    
    tdn = nc.Dataset(ncname, 'r+')       # open distributed rh ncdf file
    
    ## general fixing of dk .nc file
    tdn.renameVariable('variable','ta')   # rename variable
    xvar = tdn.variables["x"]
    xvar.longname = 'easting (meters)'
    xvar.Coordinate_System = 'UTM_NAD83_Zone11'
    yvar = tdn.variables["y"]
    yvar.longname = 'northing (meters)'
    yvar.Coordinate_System = 'UTM_NAD83_Zone11'
    tvar = tdn.variables["time"]
    tvar.units = 'hours since %s-10-01 00:00:00 -7:00' %  y
    rvar = tdn.variables["ta"]
    rvar.units = 'C'
    rvar.long_name = 'air temperature in degrees Celcius'
    ### write stations variable
    
    tdn.close()
    


