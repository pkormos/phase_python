###########################################################################
# script to analyze spatial air temperature data 
# from times with precipitation from distributed netcdf dataset files. 
# In addition, this script: 
# 
# * pulls out water year total precip
# * calculates precip. weighted ta td and tw
# * calculates and plots trends in precip. weighted ta td and tw for all of
#   rcew and elevation bands, and all water year and winter times.
# * calculates significance of trends
# * pull out elevation of phase changes each year (rain snow transition),
#   plot, fit model, and produce map of change
# 
# created - Pat Kormos November 2015
# updated - Pat Kormos September 2016
###########################################################################

import netCDF4 as nc
import numpy as np
import time
# import pyqtgraph as pg
# import matplotlib.pyplot as plt

wyoi = np.arange(1984,1993)   # water years of interest
wym  = np.roll(np.arange(1,13),3)  # build water year month vec.
# msk  = np.genfromtxt('/Users/pkormos/rcew_met_dist/terrain/rcew_10m_msk.txt', delimiter=' ',skip_header=6)
# plt.imshow(msk)
# plt.show()

# numpix = np.sum(msk)    # number of pixels in basin
sav_dir = '~/rcew_met_dist/snow_data/phase_analysis/'

## PULL OUT DATA FROM STORMS NCDF FILES, ENTIRE WATER YEAR


for y in wyoi:					# for y in wyoi:                 
    print "begin WY%s" % y      # user update

    # defined in and out files ############################################
    ppt_file = "/Volumes/LaCie/precip_cf/precip_wy%s.nc" % y
    dpt_file = "/Volumes/megadozer/CZO/dp_corrected/dp_wy%s.nc" % y
    ta_file  = "/Volumes/SHARE1/Mac_Files/team-files/Patrick/ta_not_rounded/ta_wy%s.nc" % y
    wb_file  = "/Volumes/megadozer/CZO/wb_corrected/wb_wy%s.nc" % y

    # wyt = [datenum(wyoi(y)-1,10,1):1/24:datenum(wyoi(y),9,30,23,0,0)]'; # build time vec.

    # get timesteps with precipitation
    p_ncid = nc.Dataset(ppt_file,'r')      				# open ncdf file
    p_varid = p_ncid.variables['precipitation_amount'] 	# get precip var id
    p = p_ncid.variables['raw_data']  					#
    ts = p[:] # rdmsk = (p>0)
    ts = ts.sum(axis=1)>0
    #     time_ppt = wyt(ts);                             # record times with ppt > 1mm
    print "starting ppt for WY%s" % y
    #     ppt_maps = zeros(rows,cols,length(ts),'single');    # mem for mixed precip maps.
    t = time.time()
    ppt_maps = p_varid[ts,:,:]
    t = time.time() - t
    print "time elapsed = %s" % t
    print "got all precip for WY%s. Saving images." % y

	## GET WATER YEAR total precipitation maps. 
    ppt_wytot(:,:,y) = sum(pmat.ppt_maps,3);               # get total water year precip distribution
# end
# save('ppt_wytot.mat','ppt_wytot','-v7.3')

    
#     # get dewpoint netcdf file pointers
#     d_ncid = netcdf.open(dpt_file,'NOWRITE');                   # open ncdf file
#     d_varid = netcdf.inqVarID(d_ncid,'dew_point_temperature');  # get dp var id
#     dpt_maps = zeros(rows,cols,length(ts),'single');            # mem for total precip maps.
#     disp(['starting dew point for ' num2str(wyoi(y))])          # update user
#     tic                                                         # start timer
#     for t = 1:length(ts)                                        # for each time step with preip in the water year
#         dpt_maps(:,:,t) = [netcdf.getVar(d_ncid,d_varid,[0,0,ts(t)-1],[cols,rows,1])]'; # retrieve dist. dewpt data
#     end
#     disp(['got all dewpoint for WY' num2str(wyoi(y)) '. Saving images.'])
#     save([sav_dir 'dpt_maps_' num2str(wyoi(y)) '.mat'],'dpt_maps','time_ppt','-v7.3');
#     clear dpt_maps
#     netcdf.close(d_ncid);
    
#     # get air temp netcdf file pointers
#     t_ncid = netcdf.open(ta_file,'NOWRITE');                    # open ncdf file
#     t_varid = netcdf.inqVarID(t_ncid,'surface_temperature');    # get dp var id
#     ta_maps = zeros(rows,cols,length(ts),'single');             # mem for snow maps.
#     disp(['starting air temp for ' num2str(wyoi(y))])           # update user
#     tic                                                         # start timer
#     for t = 1:length(ts)                                        # for each time step with preip in the water year
#         ta_maps(:,:,t) = [netcdf.getVar(t_ncid,t_varid,[0,0,ts(t)-1],[cols,rows,1])]'; # retrieve dist. dewpt data
#     end
#     disp(['got all air temp grids for WY' num2str(wyoi(y)) '. Saving images.'])
#     save([sav_dir 'ta_maps_' num2str(wyoi(y)) '.mat'],'ta_maps','time_ppt','-v7.3');
#     clear ta_maps
#     netcdf.close(t_ncid);
    
#     # get wet bulb temps
#     wb_ncid = netcdf.open(wb_file,'NOWRITE');                   # open ncdf file
#     wb_varid = netcdf.inqVarID(wb_ncid,'wet_bulb_temperature'); # get wb var id
#     wbt_maps = zeros(rows,cols,length(ts),'single');            # mem for wb maps
#     tic                                                         # start timer
#     for t = 1:length(ts)                                        # for each time step with preip
#         wbt_maps(:,:,t) = [netcdf.getVar(wb_ncid,wb_varid,[0,0,ts(t)-1],[cols,rows,1])]'; # retrieve dist. wbt data not masked
#     end
#     disp(['got all wbt for WY' num2str(wyoi(y)) '. Saving images.'])
#     save([sav_dir 'wbt_maps_' num2str(wyoi(y)) '.mat'],'wbt_maps','time_ppt','-v7.3');
#     clear wbt_maps
#     netcdf.close(wb_ncid);
# end


    
# ## GET WATER YEAR PRECIP WEIGHTED MEAN TA AND TD (time weighted to get ppt wt. value for each pixel)
# ###########################################################################
# load('ppt_wytot.mat')
# [msk,cols,rows,xll,yll,cellsize,nodataval] = agrid_in('~/rcew_met_dist/terrain/rcew_10m_msk.txt'); # bring in the mask file
# wyoi = 1984:2014;   # water years of interest
# for y = 1:length(wyoi)   # for each wy
#     #     y = find(wyoi==tty(y));
#     d = zeros(size(msk)); # mem for precip wt. dew point temp
#     t = zeros(size(msk)); # mem for precip wt ta
#     pmat = matfile([sav_dir 'precip_maps_' num2str(wyoi(y)) '.mat']); # pointer to precip matfile
#     tmat = matfile([sav_dir 'ta_maps_' num2str(wyoi(y)) '.mat']); # pointer to ta matfile
#     dmat = matfile([sav_dir 'dpt_maps_' num2str(wyoi(y)) '.mat']); # pointer to dewpoint matfile
#     time_ppt = pmat.time_ppt; # bring in time data
#     #     ppt_total = sum(pmat.ppt_maps,3);               # get total water year precip distribution
#     for m = 1:length(wym) # for each water year month
#         if find(month(time_ppt)==wym(m),1)>0; # if no precipitation for that month,
#             mind1 = find(month(time_ppt)==wym(m),1); # find which timesteps are in month m
#             mind2 = find(month(time_ppt)==wym(m),1,'last'); # find which timesteps are in month m
#             mp = pmat.ppt_maps(:,:,mind1:mind2); # bring in monthly ppt data
#             mt = tmat.ta_maps(:,:,mind1:mind2);  # bring in monthly ta data
#             md = dmat.dpt_maps(:,:,mind1:mind2); # bring in monthly dp data
#             ttppt = time_ppt(month(time_ppt)==wym(m));
#             for h=1:size(mp,3)
#                 #             msk_ppt = double(mp(:,:,h)>1); # make mask with where the precip is > 1mm
#                 msk_ppt = double(mp(:,:,h)>0); # make mask with where there is precip
#                 msk_wt = (mp(:,:,h) .* msk_ppt) ./ (ppt_wytot(:,:,y) .* msk_ppt); # mask ppt imgs
#                 msk_wt(isnan(msk_wt))=0;
#                 #             msk_ppt(msk_ppt==0) = nan; # make a nan mask instead of 0
#                 #             msk_wt = (mp(:,:,h) .* msk_ppt) ./ (ppt_total_1mm(:,:,h) .* msk_ppt); # mask ppt imgs
#                 # #             msk_wt(isnan(msk_wt))=0;
#                 d = md(:,:,h) .* msk_wt + d; # add up weighted dew pt temp.
#                 t = mt(:,:,h) .* msk_wt + t; # add up weighted ta
#                 #                 figure(10); clf;
#                 #                 iscet(md(:,:,h)); title(datestr(ttppt(h)),'fontsize',18);
#                 #                 subplot(3,2,1); iscet(msk_wt); title(['pptmsk ' datestr(ttppt(h))])
#                 #                 subplot(3,2,2); iscet(mp(:,:,h)); title('ppt')
#                 #                 subplot(3,2,3); iscet(md(:,:,h)); title('dewpt.')
#                 #                 subplot(3,2,4); iscet(d); title('cum_dp')
#                 #                 subplot(3,2,5); iscet(mt(:,:,h)); title('temp.')
#                 #                 subplot(3,2,6); iscet(t); title('cum_ta')
#                 #                 pause
#             end
#         end
#     end
#     save(['~/rcew_met_dist/snow_data/phase_analysis/wy' num2str(wyoi(y)) '_pptwt_ta_td.mat'],'t','d');
# end
    
# ###########################################################################

# ## TRENDS FOR WATERYEAR, ENTIRE WATERSHED (area weighted to get one value per wy)
# #  (also can do ERROR CHECKING TROUBLE YEARS (FIXING 2011 12/8/2015)
# ###########################################################################
# wyoi = 1984:2014;
# mskn = msk; mskn(msk==0)=nan; # make mask with nan's instead of zeros
# load('ppt_wytot.mat')
# p_wt_ta_rcew = nan(length(wyoi),1);
# p_wt_dp_rcew = nan(length(wyoi),1);
# # tty = [1986 2002 2011 2014];
# # for y = 1:4;#1:length(wyoi)
# tic
# for y = 1:length(wyoi)
# #     y = find(wyoi==tty(y));
#     tt = load(['wy' num2str(wyoi(y)) '_pptwt_ta_td.mat']);
#     tt_sump = ppt_wytot(:,:,y) .* mskn;     # make cells outside rcew nan
#     tt_sump(isnan(tt_sump)) = [];           # take them out and force to column
#     tt_sump = sum(tt_sump);                 # get sum of ppt in cells
#     tt_pw = ppt_wytot(:,:,y) ./ tt_sump;    # get area precip wts.
#     #     figure(3); clf
#     #     subplot(1,2,1); iscet(tt_pw); title([num2str(wyoi(y)) 'weights'])
#     #     subplot(1,2,2); iscet(tt.d); title('ppt_wtd_dpt')
#     #     pause
#     tt_pwd = tt.d .* tt_pw .* mskn;         # mult. dpt by wts, make cells outside nan
#     tt_pwd(isnan(tt_pwd)) = [];             # get rid of nans and force into column
#     p_wt_dp_rcew(y) = sum(tt_pwd);
#     tt_pwt = tt.t .* tt_pw .* mskn;         # mult. ta by wts, make cells outside nan
#     tt_pwt(isnan(tt_pwt)) = [];             # get rid of nans and force into column
#     p_wt_ta_rcew(y) = sum(tt_pwt);
# end
# clear tt*
# toc

# ###########################################################################

# ## PLOT TRENDS FROM ENTIRE WATER YEAR FOR ENTIRE RCEW
# ###########################################################################
# scrsz = get(groot,'ScreenSize');
# close
# f = figure('Position',[.1 .1 scrsz(3)*.4 scrsz(4)*.35]);

# # figure(1); clf;
# subplot(2,1,1)
# plot(wyoi,p_wt_ta_rcew,'or','linewidth',2,'markersize',14)
# title('RCEW Precipitation Weighted Air Temperature')
# ylabel('Air Temp. (C)')
# set(gca,'fontsize',16,'ylim',[-3 7])

# subplot(2,1,2)
# plot(wyoi,p_wt_dp_rcew,'ob','markerfacecolor','b','markersize',14)
# title('RCEW Precipitation Weighted Dew Point Temperature')
# ylabel('Dew Point Temp. (C)')
# set(gca,'fontsize',16,'ylim',[-3 7])

# # add qr trends
# [pwt,wts] = quantreg(wyoi',p_wt_ta_rcew,.5);
# [pwd,wds] = quantreg(wyoi',p_wt_dp_rcew,.5,1,300);
# subplot(2,1,1)
# hold on
# plot(wyoi,polyval(pwt,wyoi),'-r','linewidth',2);
# plot(wyoi,wts.yfitci(:,1),'-r')
# plot(wyoi,wts.yfitci(:,2),'-r')
# text(1981,6,['slope = ' num2str(round(pwt(1),2))],'FontSize',16,'color','r')

# subplot(2,1,2)
# hold on
# plot(wyoi,polyval(pwd,wyoi),'-b','linewidth',2);
# plot(wyoi,wds.yfitci(:,1),'-b')
# plot(wyoi,wds.yfitci(:,2),'-b')
# text(1981,6,['slope = ' num2str(round(pwd(1),2))],'FontSize',16,'color','b')

# # autocorrelation
# rsd = p_wt_dp_rcew' - polyval(pwd,wyoi); # residuals for Td trend
# rst = p_wt_ta_rcew' - polyval(pwt,wyoi); # residuals for Ta trend
# r1d = autocorr(rsd,1); ac1d = abs(r1d(2)); # get serial corr. of resids.
# r1t = autocorr(rst,1); ac1t = abs(r1t(2)); # get serial corr. of resids.
# # figure(3); clf
# # plot(rsd,'o')
# # plot(rst,'o')
# # plot(rsd(2:end),rsd(1:end-1),'o')
# # plot(rst(2:end),rst(1:end-1),'o')


# # effective sample size, assuming lag 1, WMO 1966; Dawdy and Matalas 1964
# ned = length(rsd).*((1-ac1d)/(1+ac1d)); 
# net = length(rst).*((1-ac1t)/(1+ac1t));

# # effective sample size, Nychka et al., 2000
# ned_n = length(rsd) * (1-ac1d-(0.68/sqrt(length(rsd))))/(1+ac1d+(0.68/sqrt(length(rsd))));
# net_n = length(rst) * (1-ac1t-(0.68/sqrt(length(rst))))/(1+ac1t+(0.68/sqrt(length(rst))));

# # adjust significance of dewpoint temp trend for auto corr.
# t_orig_d = pwd(1)/wds.pse(1); # slope divided by standard error gives original t value
# p_orit_d = 2*(1-tcdf(abs(t_orig_d),length(rsd)-2)) # original p-value
# t_adj_d = t_orig_d*(sqrt(ned-2))/(sqrt(length(rsd)-2)); # adjusted t value for effective sample size and df
# p_adj_d = tcdf(t_adj_d,ned-2); # not converted adjusted p value
# if p_adj_d < 0.5;
#     p_adj_d = p_adj_d  * 2        # convert to p-values
# else
#     p_adj_d = (1-p_adj_d)  * 2      # convert to pvalues used by se="nid
# end
# subplot(2,1,2)
# text(1981,5,['p-value = ' num2str(round(p_adj_d,3))],'FontSize',16,'color','b')


# # adjust significance of air temp trend for auto corr.
# t_orig_t = pwt(1)/wts.pse(1); # slope divided by standard error gives original t value
# p_orit_t = 2*(1-tcdf(abs(t_orig_t),length(rst)-2)) # original p-value
# t_adj_t = t_orig_t*(sqrt(net-2))/(sqrt(length(rst)-2)); # adjusted t value for effective sample size and df
# p_adj_t = tcdf(t_adj_t,net-2); # not converted adjusted p value
# if p_adj_t < 0.5;
#     p_adj_t = p_adj_t  * 2        # convert to p-values
# else
#     p_adj_t = (1-p_adj_t)  * 2      # convert to pvalues used by se="nid
# end
# subplot(2,1,1)
# text(1981,5,['p-value = ' num2str(round(p_adj_t,3))],'FontSize',16,'color','r')

# set(gcf, 'Color', 'w','PaperPositionMode', 'auto');
# # print -depsc2 -loose '~/Documents/papers/SMNT_DATA/PPT_BAR.eps'
# print -djpeg -loose '~/Documents/papers/precip_dist_30yr_chng/figs/ppt_wt_ta_td_trends.jpg'


# ###########################################################################

# ## get trends for entire wateryear for elevation bands 
# ###########################################################################
# # build hypsometric relationship
# [dem,xvec,yvec] = agrid_inv('~/rcew_met_dist/terrain/rcew_10m.txt'); # bring in dem
# mskn = msk; mskn(msk==0)=nan; # make mask with nan's instead of zeros
# dem1 = dem.*mskn;       # exclude elevations outside of the boundary
# dem1 = dem1(:);         # force to column
# dem1(isnan(dem1)) = []; # get rid of outside the boundary
# dem1 = sort(dem1);      # sort elevations lowest to highest
# x = (1:length(dem1))./length(dem1);    # get percent of watershed area vector
# # split up watershed into elevation regions (bands)
# ione_third = searchclosest1(x,1/3);
# eone_third = dem1(ione_third);         # elevation that splits the lower thirds
# itwo_third = searchclosest1(x,2/3);
# etwo_third = dem1(itwo_third);         # elevation that splits the upper thirds
# msk_ut = double(dem>=etwo_third); msk_ut(msk_ut==0)=nan;
# msk_mt = double(dem<etwo_third & dem >= eone_third); msk_mt(msk_mt==0)=nan;
# msk_lt = double(dem < eone_third); msk_lt(msk_lt==0)=nan; msk_lt=msk_lt.*mskn;
# #mem
# p_wt_ta_ut = nan(length(wyoi),1); # up third
# p_wt_ta_mt = nan(length(wyoi),1); # mid third
# p_wt_ta_lt = nan(length(wyoi),1); # two third
# p_wt_dp_ut = nan(length(wyoi),1);
# p_wt_dp_mt = nan(length(wyoi),1);
# p_wt_dp_lt = nan(length(wyoi),1);
# load('~/rcew_met_dist/snow_data/phase_analysis/ppt_wytot.mat');
# for y = 1:length(wyoi)
#     tt = load(['wy' num2str(wyoi(y)) '_pptwt_ta_td.mat']);
#     tt_tpmap_u = ppt_wytot(:,:,y) .* msk_ut; # make cells outside up third nan
#     tt_sump_u = tt_tpmap_u;
#     tt_sump_u(isnan(tt_tpmap_u)) = [];           # take them out and force to column
#     tt_sump_u = sum(tt_sump_u);                 # get sum of ppt in cells
#     tt_pw_u = tt_tpmap_u ./ tt_sump_u;# get area precip wts.
#     tt_pwd_u = tt.d .* tt_pw_u;         # mult. dpt by wts, make cells outside nan
#     tt_pwd_u(isnan(tt_pwd_u)) = [];             # get rid of nans and force into column
#     p_wt_dp_ut(y) = sum(tt_pwd_u);
#     tt_pwt_u = tt.t .* tt_pw_u;         # mult. ta by wts, make cells outside nan
#     tt_pwt_u(isnan(tt_pwt_u)) = [];             # get rid of nans and force into column
#     p_wt_ta_ut(y) = sum(tt_pwt_u);
    
#     tt_tpmap_m = ppt_wytot(:,:,y) .* msk_mt; # make cells outside up third nan
#     tt_sump_m = tt_tpmap_m;
#     tt_sump_m(isnan(tt_tpmap_m)) = [];           # take them out and force to column
#     tt_sump_m = sum(tt_sump_m);                 # get sum of ppt in cells
#     tt_pw_m = tt_tpmap_m ./ tt_sump_m;# get area precip wts.
#     tt_pwd_m = tt.d .* tt_pw_m;         # mult. dpt by wts, make cells outside nan
#     tt_pwd_m(isnan(tt_pwd_m)) = [];             # get rid of nans and force into column
#     p_wt_dp_mt(y) = sum(tt_pwd_m);
#     tt_pwt_m = tt.t .* tt_pw_m;         # mult. ta by wts, make cells outside nan
#     tt_pwt_m(isnan(tt_pwt_m)) = [];             # get rid of nans and force into column
#     p_wt_ta_mt(y) = sum(tt_pwt_m);
    
#     tt_tpmap_l = ppt_wytot(:,:,y) .* msk_lt; # make cells outside up third nan
#     tt_sump_l = tt_tpmap_l;
#     tt_sump_l(isnan(tt_tpmap_l)) = [];           # take them out and force to column
#     tt_sump_l = sum(tt_sump_l);                 # get sum of ppt in cells
#     tt_pw_l = tt_tpmap_l ./ tt_sump_l;# get area precip wts.
#     tt_pwd_l = tt.d .* tt_pw_l;         # mult. dpt by wts, make cells outside nan
#     tt_pwd_l(isnan(tt_pwd_l)) = [];             # get rid of nans and force into column
#     p_wt_dp_lt(y) = sum(tt_pwd_l);
#     tt_pwt_l = tt.t .* tt_pw_l;         # mult. ta by wts, make cells outside nan
#     tt_pwt_l(isnan(tt_pwt_l)) = [];             # get rid of nans and force into column
#     p_wt_ta_lt(y) = sum(tt_pwt_l);

# end
# clear tt*
# [p_wt_ta_ut p_wt_ta_mt p_wt_ta_lt]

# ###########################################################################

# ## plot TA AND DEW PT TEMP MEANS FROM elevation bands for entire water year
# ###########################################################################
# pwyoi = wyoi;
# # pwyoi(ismember(wyoi,[1986 2002 2011 2014]))=[];
# pp_wt_ta_ut = p_wt_ta_ut;
# # pp_wt_ta_ut(ismember(wyoi,[1986 2002 2011 2014]))=[];
# pp_wt_ta_mt = p_wt_ta_mt;
# # pp_wt_ta_mt(ismember(wyoi,[1986 2002 2011 2014]))=[];
# pp_wt_ta_lt = p_wt_ta_lt;
# # pp_wt_ta_lt(ismember(wyoi,[1986 2002 2011 2014]))=[];
# pp_wt_dp_ut=p_wt_dp_ut;
# # pp_wt_dp_ut(ismember(wyoi,[1986 2002 2011 2014]))=[];
# pp_wt_dp_mt=p_wt_dp_mt;
# # pp_wt_dp_mt(ismember(wyoi,[1986 2002 2011 2014]))=[];
# pp_wt_dp_lt=p_wt_dp_lt;
# # pp_wt_dp_lt(ismember(wyoi,[1986 2002 2011 2014]))=[];

# close
# f = figure('Position',[.1 .1 scrsz(3)*.4 scrsz(4)*.6]);

# # figure(31); clf
# subplot(3,1,1);
# plot(pwyoi,[pp_wt_dp_ut,pp_wt_ta_ut],'o')
# title(['Upper Third of RCEW: (>' num2str(etwo_third) 'm)'])
# legend('Ta','Dew Point T')
# hold on
# # add trends
# # quantile regression of trends
# [pud,uds] = quantreg(pwyoi',pp_wt_dp_ut,.5);
# [put,uts] = quantreg(pwyoi',pp_wt_ta_ut,.5);
# plot(pwyoi,polyval(pud,pwyoi),'-b','linewidth',2);
# plot(pwyoi,uds.yfitci(:,1),'-b')
# plot(pwyoi,uds.yfitci(:,2),'-b')
# text(1981,-2,['slope = ' num2str(pud(1))],'FontSize',16,'color','b')
# plot(pwyoi,polyval(put,pwyoi),'-r','linewidth',2);
# plot(pwyoi,uts.yfitci(:,1),'-r')
# plot(pwyoi,uts.yfitci(:,2),'-r')
# text(1981,2.5,['slope = ' num2str(put(1))],'FontSize',16,'color','r')
# set(gca,'fontsize',15,'ylim',[-6 4])
# hold off

# subplot(3,1,2);
# plot(pwyoi,[pp_wt_ta_mt pp_wt_dp_mt],'o')
# title(['Middle Third of RCEW: (' num2str(eone_third) '-' num2str(etwo_third) 'm)'])
# legend('Ta','Dew Point T')
# hold on
# # add trends
# # quantile regression of trends
# [pmd,mds] = quantreg(pwyoi',pp_wt_dp_mt,.5,1,750);
# [pmt,mts] = quantreg(pwyoi',pp_wt_ta_mt,.5,1,750);
# plot(pwyoi,polyval(pmd,pwyoi),'-b','linewidth',2);
# plot(pwyoi,mds.yfitci(:,1),'-b')
# plot(pwyoi,mds.yfitci(:,2),'-b')
# text(1981,-1,['slope = ' num2str(pmd(1))],'FontSize',16,'color','b')
# plot(pwyoi,polyval(pmt,pwyoi),'-r','linewidth',2);
# plot(pwyoi,mts.yfitci(:,1),'-r')
# plot(pwyoi,mts.yfitci(:,2),'-r')
# text(1981,5,['slope = ' num2str(pmt(1))],'FontSize',16,'color','r')
# set(gca,'fontsize',15)
# hold off

# subplot(3,1,3);
# plot(pwyoi,[pp_wt_ta_lt pp_wt_dp_lt],'o')
# title(['Lower Third of RCEW: (<' num2str(eone_third) 'm)'])
# legend('Ta','Dew Point T')
#     hold on
# # add trends
# # quantile regression of trends
# [pld,lds] = quantreg(pwyoi',pp_wt_dp_lt,.5,1,500);
# [plt,lts] = quantreg(pwyoi',pp_wt_ta_lt,.5,1,500);
# plot(pwyoi,polyval(pld,pwyoi),'-b','linewidth',2);
# plot(pwyoi,lds.yfitci(:,1),'-b')
# plot(pwyoi,lds.yfitci(:,2),'-b')
# text(1981,0,['slope = ' num2str(pld(1))],'FontSize',16,'color','b')
# plot(pwyoi,polyval(plt,pwyoi),'-r','linewidth',2);
# plot(pwyoi,lts.yfitci(:,1),'-r')
# plot(pwyoi,lts.yfitci(:,2),'-r')
# text(1981,6,['slope = ' num2str(plt(1))],'FontSize',16,'color','r')
# set(gca,'fontsize',15,'ylim',[-1 10])
# hold off

# ## get and adjust p values for effective sample size (correct for autocorrelation)
# # effective sample sizes, assuming lag 1, WMO 1966; Dawdy and Matalas 1964

# # get residuals for dewpoint
# rsdu = p_wt_dp_ut' - polyval(pud,wyoi); # residuals for Td trend
# rsdm = p_wt_dp_mt' - polyval(pmd,wyoi); # residuals for Td trend
# rsdl = p_wt_dp_lt' - polyval(pld,wyoi); # residuals for Td trend
# # get residuals for air temp
# rstu = p_wt_ta_ut' - polyval(put,wyoi); # residuals for Ta trend
# rstm = p_wt_ta_mt' - polyval(pmt,wyoi); # residuals for Ta trend
# rstl = p_wt_ta_lt' - polyval(plt,wyoi); # residuals for Ta trend
# # get autocorr for dewpoint
# r1du = autocorr(rsdu,1); ac1du = abs(r1du(2)); # get serial corr. of resids.
# r1dm = autocorr(rsdm,1); ac1dm = abs(r1dm(2)); # get serial corr. of resids.
# r1dl = autocorr(rsdl,1); ac1dl = abs(r1dl(2)); # get serial corr. of resids.
# # get autocorr for air temp
# r1tu = autocorr(rstu,1); ac1tu = abs(r1tu(2)); # get serial corr. of resids.
# r1tm = autocorr(rstm,1); ac1tm = abs(r1tm(2)); # get serial corr. of resids.
# r1tl = autocorr(rstl,1); ac1tl = abs(r1tl(2)); # get serial corr. of resids.
# # get effective sample size for dewpoint and air temp.
# nedu = length(wyoi).*((1-ac1du)/(1+ac1du)); 
# nedm = length(wyoi).*((1-ac1dm)/(1+ac1dm)); 
# nedl = length(wyoi).*((1-ac1dl)/(1+ac1dl)); 
# netu = length(wyoi).*((1-ac1tu)/(1+ac1tu));
# netm = length(wyoi).*((1-ac1tm)/(1+ac1tm));
# netl = length(wyoi).*((1-ac1tl)/(1+ac1tl));

# # adjust significance of dewpoint temp trend for auto corr.
# t_orig_du = pud(1)/uds.pse(1); # slope divided by standard error gives original t value
# t_orig_dm = pmd(1)/mds.pse(1); # slope divided by standard error gives original t value
# t_orig_dl = pld(1)/lds.pse(1); # slope divided by standard error gives original t value

# p_orit_du = 2*(1-tcdf(abs(t_orig_du),length(wyoi)-2)) # original p-value
# p_orit_dm = 2*(1-tcdf(abs(t_orig_dm),length(wyoi)-2)) # original p-value
# p_orit_dl = 2*(1-tcdf(abs(t_orig_dl),length(wyoi)-2)) # original p-value

# t_adj_du = t_orig_du*(sqrt(nedu-2))/(sqrt(length(wyoi)-2)); # adjusted t value for effective sample size and df
# t_adj_dm = t_orig_dm*(sqrt(nedm-2))/(sqrt(length(wyoi)-2)); # adjusted t value for effective sample size and df
# t_adj_dl = t_orig_dl*(sqrt(nedl-2))/(sqrt(length(wyoi)-2)); # adjusted t value for effective sample size and df

# p_adj_du = tcdf(t_adj_du,nedu-2); # not converted adjusted p value
# p_adj_dm = tcdf(t_adj_dm,nedm-2); # not converted adjusted p value
# p_adj_dl = tcdf(t_adj_dl,nedl-2); # not converted adjusted p value

# if p_adj_du < 0.5;
#     p_adj_du = p_adj_du  * 2        # convert to p-values
# else
#     p_adj_du = (1-p_adj_du)  * 2      # convert to pvalues used by se="nid
# end
# if p_adj_dm < 0.5;
#     p_adj_dm = p_adj_dm  * 2        # convert to p-values
# else
#     p_adj_dm = (1-p_adj_dm)  * 2      # convert to pvalues used by se="nid
# end
# if p_adj_dl < 0.5;
#     p_adj_dl = p_adj_dl  * 2        # convert to p-values
# else
#     p_adj_dl = (1-p_adj_dl)  * 2      # convert to pvalues used by se="nid
# end

# # adjust significance of air temp trend for auto corr.
# t_orig_tu = put(1)/uts.pse(1); # slope divided by standard error gives original t value
# t_orig_tm = pmt(1)/mts.pse(1); # slope divided by standard error gives original t value
# t_orig_tl = plt(1)/lts.pse(1); # slope divided by standard error gives original t value

# p_orit_tu = 2*(1-tcdf(abs(t_orig_tu),length(wyoi)-2)) # original p-value
# p_orit_tm = 2*(1-tcdf(abs(t_orig_tm),length(wyoi)-2)) # original p-value
# p_orit_tl = 2*(1-tcdf(abs(t_orig_tl),length(wyoi)-2)) # original p-value

# t_adj_tu = t_orig_tu*(sqrt(netu-2))/(sqrt(length(wyoi)-2)); # adjusted t value for effective sample size and df
# t_adj_tm = t_orig_tm*(sqrt(netm-2))/(sqrt(length(wyoi)-2)); # adjusted t value for effective sample size and df
# t_adj_tl = t_orig_tl*(sqrt(netl-2))/(sqrt(length(wyoi)-2)); # adjusted t value for effective sample size and df

# p_adj_tu = tcdf(t_adj_tu,netu-2); # not converted adjusted p value
# p_adj_tm = tcdf(t_adj_tm,netm-2); # not converted adjusted p value
# p_adj_tl = tcdf(t_adj_tl,netl-2); # not converted adjusted p value

# if p_adj_tu < 0.5;
#     p_adj_tu = p_adj_tu  * 2        # convert to p-values
# else
#     p_adj_tu = (1-p_adj_tu)  * 2      # convert to pvalues used by se="nid
# end
# if p_adj_tm < 0.5;
#     p_adj_tm = p_adj_tm * 2        # convert to p-values
# else
#     p_adj_tm = (1-p_adj_tm)  * 2      # convert to pvalues used by se="nid
# end
# if p_adj_tl < 0.5;
#     p_adj_tl = p_adj_tl  * 2        # convert to p-values
# else
#     p_adj_tl = (1-p_adj_tl)  * 2      # convert to pvalues used by se="nid
# end

# ## figure plotter for elevation bands precipitation weighted air temp and dew pt temp.
#   [le,bo,wi,he] = subplot_spacing(4,2,.06,.05,.01,.09,.01,.01);
# # [le,bo,wi,he] = subplot_spacing(prows,pcols,bot_brd,top_brd,rit_brd,lef_brd,v_space,h_space)
# scrsz = get(groot,'ScreenSize');

# fs = 14; # axis font size
# afs = 15; # annotation font size
# tfs = 16; # title font size
# ylfs = 16; # ylabel font size
# yl = [-5 9];
# xl = [1983 2015];
# xt = 1985:5:2010;
# yt = -4:2:8;
# ms = 10; # marker size
# close(f);
# f = figure('Position',[.1 .1 scrsz(3)*.4 scrsz(4)*.6]);

# # WHOLE CATCHMENT DEW POINT TEMP. #########################################
# subplot('position',[le(1) bo(1) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,p_wt_dp_rcew,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# title('Precipitation-Weighted Dew Point Temperature','fontsize',tfs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'Whole Catchment';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pwd,wyoi),'-b','linewidth',2);
# plot(wyoi,wds.yfitci(:,1),'-b')
# plot(wyoi,wds.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pwd(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_d,3))],'FontSize',afs,'color','b')

# # WHOLE CATCHMENT AIR TEMP. ###############################################
# subplot('position',[le(2) bo(2) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# hold on
# plot(wyoi,p_wt_ta_rcew,'or','linewidth',2,'markersize',ms)
# title('Precipitation-Weighted Air Temperature','fontsize',tfs)
# plot(wyoi,polyval(pwt,wyoi),'-r','linewidth',2);
# plot(wyoi,wts.yfitci(:,1),'-r')
# plot(wyoi,wts.yfitci(:,2),'-r')
# text(1985,-2,['slope = ' num2str(round(pwt(1),2))],'FontSize',afs,'color','r')
# text(1985,-3.25,['p-value = ' num2str(round(p_adj_t,3))],'FontSize',afs,'color','r')

# # UPPER ELEV. DEW POINT TEMP. #############################################
# subplot('position',[le(3) bo(3) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(pwyoi,pp_wt_dp_ut,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'High Elevation';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pud,wyoi),'-b','linewidth',2);
# plot(wyoi,uds.yfitci(:,1),'-b')
# plot(wyoi,uds.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pud(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_du,3))],'FontSize',afs,'color','b')

# # UPPER ELEV. AIR TEMP. ###################################################
# subplot('position',[le(4) bo(4) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(pwyoi,pp_wt_ta_ut,'or','linewidth',2,'markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# plot(wyoi,polyval(put,wyoi),'-r','linewidth',2);
# plot(wyoi,uts.yfitci(:,1),'-r')
# plot(wyoi,uts.yfitci(:,2),'-r')
# text(1985,-2,['slope = ' num2str(round(put(1),2))],'FontSize',afs,'color','r')
# text(1985,-3.25,['p-value = ' num2str(round(p_adj_tu,3))],'FontSize',afs,'color','r')

# # MID ELEV. DEW POINT TEMP. #############################################
# subplot('position',[le(5) bo(5) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(pwyoi,pp_wt_dp_mt,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'Mid. Elevation';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pmd,wyoi),'-b','linewidth',2);
# plot(wyoi,mds.yfitci(:,1),'-b')
# plot(wyoi,mds.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pmd(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_dm,3))],'FontSize',afs,'color','b')

# # MID ELEV. AIR TEMP. ###################################################
# subplot('position',[le(6) bo(6) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(pwyoi,pp_wt_ta_mt,'or','linewidth',2,'markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# plot(wyoi,polyval(pmt,wyoi),'-r','linewidth',2);
# plot(wyoi,mts.yfitci(:,1),'-r')
# plot(wyoi,mts.yfitci(:,2),'-r')
# text(1985,-2,['slope = ' num2str(round(pmt(1),2))],'FontSize',afs,'color','r')
# text(1985,-3.25,['p-value = ' num2str(round(p_adj_tm,3))],'FontSize',afs,'color','r')

# # LOW ELEV. DEW POINT TEMP. #############################################
# subplot('position',[le(7) bo(7) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(pwyoi,pp_wt_dp_lt,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'Low Elevation';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pld,wyoi),'-b','linewidth',2);
# plot(wyoi,lds.yfitci(:,1),'-b')
# plot(wyoi,lds.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pld(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_dl,3))],'FontSize',afs,'color','b')

# # LOW ELEV. AIR TEMP. ###################################################
# subplot('position',[le(8) bo(8) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(pwyoi,pp_wt_ta_lt,'or','linewidth',2,'markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# plot(wyoi,polyval(plt,wyoi),'-r','linewidth',2);
# plot(wyoi,lts.yfitci(:,1),'-r')
# plot(wyoi,lts.yfitci(:,2),'-r')
# text(1985,-2,['slope = ' num2str(round(plt(1),2))],'FontSize',afs,'color','r')
# text(1985,-3.25,['p-value = ' num2str(round(p_adj_tl,3))],'FontSize',afs,'color','r')

# ## figure saver
# set(gcf, 'Color', 'w','PaperPositionMode', 'auto');
# print -djpeg -loose -r300 '~//Documents/papers/precip_dist_30yr_chng/figs/ppt_wt_ta_td_elev_trends.jpg'

# ###########################################################################

# ## GET NEW PRECIP WINTER TOTALS (EXTRACT VALUES)
# ###########################################################################
# # wwym = [12 1:4];    # define winter water year months
# # # wppt_total_1mm = nan(size(msk,1),size(msk,2),length(wyoi));
# # wppt_total = nan(size(msk,1),size(msk,2),length(wyoi));
# # for y = 1:length(wyoi)   # for each wy
# #     tt = matfile([sav_dir 'precip_maps_' num2str(wyoi(y)) '.mat']);
# #     #     wppt_total_1mm(:,:,y) = sum(ppt_maps(:,:,ismember(month(time_ppt),wwym)),3);
# #     si = find(ismember(month(tt.time_ppt),wwym),1,'first');
# #     ei = find(ismember(month(tt.time_ppt),wwym),1,'last');
# #     wppt_total(:,:,y) = sum(tt.ppt_maps(:,:,si:ei),3);
# # end 
# # # save('~/rcew_met_dist/snow_data/phase_analysis/ppt_wtot_1mm.mat','wppt_total_1mm')
# # save('~/rcew_met_dist/snow_data/phase_analysis/ppt_wtot.mat','wppt_total')
# load('~/rcew_met_dist/snow_data/phase_analysis/ppt_wtot.mat','wppt_total')

# ###########################################################################

# ## GET PRECIP WEIGHTED MEAN TA AND TD FOR WINTER DEC-APR (EXTRACT VALUES)
# ###########################################################################
# wyoi = 1984:2014;   # water years of interest
# wwym = [12 1:4];    # define winter water year months
# load ppt_wtot.mat # load winter ppt totals >1mm.
# for y = 1:length(wyoi)   # for each wy
#     wd = zeros(size(msk)); # mem for precip wt. dew point temp
#     wt = zeros(size(msk)); # mem for precip wt ta
#     pmat = matfile([sav_dir 'precip_maps_' num2str(wyoi(y)) '.mat']); # pointer to precip matfile
#     tmat = matfile([sav_dir 'ta_maps_' num2str(wyoi(y)) '.mat']); # pointer to ta matfile
#     dmat = matfile([sav_dir 'dpt_maps_' num2str(wyoi(y)) '.mat']); # pointer to dewpoint matfile
#     time_ppt = pmat.time_ppt; # bring in time data
#     for m = 1:length(wwym) # for each water year month
#         if isempty(find(month(time_ppt)==wwym(m),1)); # if no precipitation for that month,
#             continue # go to the next month
#         end
#         mind1 = find(month(time_ppt)==wwym(m),1); # find which timesteps are in month m
#         mind2 = find(month(time_ppt)==wwym(m),1,'last'); # find which timesteps are in month m
#         mp = pmat.ppt_maps(:,:,mind1:mind2); # bring in monthly ppt data
#         mt = tmat.ta_maps(:,:,mind1:mind2);  # bring in monthly ta data
#         md = dmat.dpt_maps(:,:,mind1:mind2); # bring in monthly dp data
#         ttppt = time_ppt(month(time_ppt)==wwym(m));
#         for h=1:size(mp,3)
#             msk_ppt = double(mp(:,:,h)>0); # make mask with where the precip is > 0
#             msk_wt = (mp(:,:,h) .* msk_ppt) ./ (wppt_total(:,:,y) .* msk_ppt); # mask ppt imgs
#             msk_wt(isnan(msk_wt))=0;
#             wd = md(:,:,h) .* msk_wt + wd; # add up weighted dew pt temp.
#             wt = mt(:,:,h) .* msk_wt + wt; # add up weighted ta
# #             figure(10); clf;
# #             iscet(md(:,:,h)); title(datestr(ttppt(h)),'fontsize',18);
# #             subplot(3,2,1); iscet(msk_ppt); title(['pptmsk ' datestr(ttppt(h))])
# #             subplot(3,2,2); iscet(mp(:,:,h)); title('ppt')
# #             subplot(3,2,3); iscet(md(:,:,h)); title('dewpt.')
# #             subplot(3,2,4); iscet(wd); title('cum_dp') 
# #             subplot(3,2,5); iscet(mt(:,:,h)); title('temp.')
# #             subplot(3,2,6); iscet(wt); title('cum_ta')
# #             pause
#         end
#     end            
#     save(['~/rcew_met_dist/snow_data/phase_analysis/wy' num2str(wyoi(y)) 'w_pptwt_ta_td.mat'],'wt','wd');
# end

# ###########################################################################

# ## GET TRENDS FOR WINTER TIME, ENTIRE WATERSHED 
# ###########################################################################
# load('~/rcew_met_dist/snow_data/phase_analysis/ppt_wtot.mat')
# mskn = msk; mskn(msk==0)=nan; # make mask with nan's instead of zeros
# wp_wt_ta_rcew = nan(length(wyoi),1);
# wp_wt_dp_rcew = nan(length(wyoi),1);
# for y = 1:length(wyoi)
#     tt = load(['wy' num2str(wyoi(y)) 'w_pptwt_ta_td.mat']);
#     tt_sump = wppt_total(:,:,y) .* mskn; # make cells outside rcew nan
#     tt_sump(isnan(tt_sump)) = [];           # take them out and force to column
#     tt_sump = sum(tt_sump);                 # get sum of ppt in cells
#     tt_pw = wppt_total(:,:,y) ./ tt_sump;# get area precip wts.
# #         figure(3); clf
# #         subplot(1,2,1); iscet(tt_pw); title([num2str(wyoi(y)) 'weights'])
# #         subplot(1,2,2); iscet(tt.wd); title('ppt_wtd_dpt')
# #         pause
#     tt_pwd = tt.wd .* tt_pw .* mskn;         # mult. dpt by wts, make cells outside nan
#     tt_pwd(isnan(tt_pwd)) = [];             # get rid of nans and force into column
#     wp_wt_dp_rcew(y) = sum(tt_pwd);
#     tt_pwt = tt.wt .* tt_pw .* mskn;         # mult. ta by wts, make cells outside nan
#     tt_pwt(isnan(tt_pwt)) = [];             # get rid of nans and force into column
#     wp_wt_ta_rcew(y) = sum(tt_pwt);
# end
# clear tt*

# ###########################################################################

# ## PLOT TRENDS FROM WINTER FOR ENTIRE RCEW
# ###########################################################################
# figure(1); clf;
# subplot(2,1,1)
# plot(wyoi,wp_wt_ta_rcew,'o')
# title('RCEW Winter Precipitation Weighted Winter Air Temperature')
# ylabel('Air Temp. (C)')
# set(gca,'fontsize',16,'ylim',[-3 3])
# subplot(2,1,2)
# plot(wyoi,wp_wt_dp_rcew,'o')
# title('RCEW Winter Precipitation Weighted Winter Dew Point Temperature')
# ylabel('Dew Point Temp. (C)')
# set(gca,'fontsize',16)

# ###########################################################################

# ## GET WINTER PRECIP WT. TA AND DPT FOR ELEVATION BANDS FOR EACH YEAR
# ###########################################################################
# # build hypsometric relationship
# [dem,xvec,yvec] = agrid_inv('/Volumes/HD1/CZO/spatial_resolution_compare/jan2002_010m/rcew_10m.txt'); # bring in dem
# mskn = msk; mskn(msk==0)=nan; # make mask with nan's instead of zeros
# dem1 = dem.*mskn;       # exclude elevations outside of the boundary
# dem1 = dem1(:);         # force to column
# dem1(isnan(dem1)) = []; # get rid of outside the boundary
# dem1 = sort(dem1);      # sort elevations lowest to highest
# x = (1:length(dem1))./length(dem1);    # get percent of watershed area vector
# # split up watershed into elevation regions (bands)
# ione_third = searchclosest1(x,1/3);
# eone_third = dem1(ione_third);         # elevation that splits the lower thirds
# itwo_third = searchclosest1(x,2/3);
# etwo_third = dem1(itwo_third);         # elevation that splits the upper thirds
# msk_ut = double(dem>=etwo_third); msk_ut(msk_ut==0)=nan;
# msk_mt = double(dem<etwo_third & dem >= eone_third); msk_mt(msk_mt==0)=nan;
# msk_lt = double(dem < eone_third); msk_lt(msk_lt==0)=nan; msk_lt=msk_lt.*mskn;
# #mem
# wp_wt_ta_ut = nan(length(wyoi),1); # up third
# wp_wt_ta_mt = nan(length(wyoi),1); # mid third
# wp_wt_ta_lt = nan(length(wyoi),1); # two third
# wp_wt_dp_ut = nan(length(wyoi),1);
# wp_wt_dp_mt = nan(length(wyoi),1);
# wp_wt_dp_lt = nan(length(wyoi),1);
# load('~/rcew_met_dist/snow_data/phase_analysis/ppt_wtot.mat');
# for y = 1:length(wyoi)
#     tt = load(['wy' num2str(wyoi(y)) 'w_pptwt_ta_td.mat']);
#     tt_tpmap_u = wppt_total(:,:,y) .* msk_ut; # make cells outside up third nan
#     tt_sump_u = tt_tpmap_u;
#     tt_sump_u(isnan(tt_tpmap_u)) = [];           # take them out and force to column
#     tt_sump_u = sum(tt_sump_u);                 # get sum of ppt in cells
#     tt_pw_u = tt_tpmap_u ./ tt_sump_u;# get area precip wts.
#     tt_pwd_u = tt.wd .* tt_pw_u;         # mult. dpt by wts, make cells outside nan
#     tt_pwd_u(isnan(tt_pwd_u)) = [];             # get rid of nans and force into column
#     wp_wt_dp_ut(y) = sum(tt_pwd_u);
#     tt_pwt_u = tt.wt .* tt_pw_u;         # mult. ta by wts, make cells outside nan
#     tt_pwt_u(isnan(tt_pwt_u)) = [];             # get rid of nans and force into column
#     wp_wt_ta_ut(y) = sum(tt_pwt_u);
    
#     tt_tpmap_m = wppt_total(:,:,y) .* msk_mt; # make cells outside up third nan
#     tt_sump_m = tt_tpmap_m;
#     tt_sump_m(isnan(tt_tpmap_m)) = [];           # take them out and force to column
#     tt_sump_m = sum(tt_sump_m);                 # get sum of ppt in cells
#     tt_pw_m = tt_tpmap_m ./ tt_sump_m;# get area precip wts.
#     tt_pwd_m = tt.wd .* tt_pw_m;         # mult. dpt by wts, make cells outside nan
#     tt_pwd_m(isnan(tt_pwd_m)) = [];             # get rid of nans and force into column
#     wp_wt_dp_mt(y) = sum(tt_pwd_m);
#     tt_pwt_m = tt.wt .* tt_pw_m;         # mult. ta by wts, make cells outside nan
#     tt_pwt_m(isnan(tt_pwt_m)) = [];             # get rid of nans and force into column
#     wp_wt_ta_mt(y) = sum(tt_pwt_m);
    
#     tt_tpmap_l = wppt_total(:,:,y) .* msk_lt; # make cells outside up third nan
#     tt_sump_l = tt_tpmap_l;
#     tt_sump_l(isnan(tt_tpmap_l)) = [];           # take them out and force to column
#     tt_sump_l = sum(tt_sump_l);                 # get sum of ppt in cells
#     tt_pw_l = tt_tpmap_l ./ tt_sump_l;# get area precip wts.
#     tt_pwd_l = tt.wd .* tt_pw_l;         # mult. dpt by wts, make cells outside nan
#     tt_pwd_l(isnan(tt_pwd_l)) = [];             # get rid of nans and force into column
#     wp_wt_dp_lt(y) = sum(tt_pwd_l);
#     tt_pwt_l = tt.wt .* tt_pw_l;         # mult. ta by wts, make cells outside nan
#     tt_pwt_l(isnan(tt_pwt_l)) = [];             # get rid of nans and force into column
#     wp_wt_ta_lt(y) = sum(tt_pwt_l);

# end
# clear tt*
# [wyoi' wp_wt_ta_ut wp_wt_ta_mt wp_wt_ta_lt]
# # [wyoi' wp_wt_dp_ut wp_wt_dp_mt wp_wt_dp_lt]

# ###########################################################################

# ## fit trends to winter time ta and td wy1984-wy2014
# ###########################################################################
# [pwdw,wdsw] = quantreg(wyoi',wp_wt_dp_rcew,.5,1,300);  # quantile regression dewpoint temp, rcew
# rsdw = wp_wt_dp_rcew' - polyval(pwdw,wyoi);            # residuals for Td trend
# r1dw = autocorr(rsdw,1); ac1dw = abs(r1dw(2));         # get serial corr. of resids.
# nedw = length(rsdw).*((1-ac1dw)/(1+ac1dw));            # effective sample size, assuming lag 1, WMO 1966; Dawdy and Matalas 1964
# t_orig_dw = pwdw(1)/wdsw.pse(1);                       # slope divided by standard error gives original t value
# # p_orit_dw = 2*(1-tcdf(abs(t_orig_dw),length(rsdw)-2));  # original p-value
# t_adj_dw = t_orig_dw*(sqrt(nedw-2))/(sqrt(length(rsdw)-2)); # adjusted t value for effective sample size and df
# p_adj_dw = tcdf(t_adj_dw,nedw-2);                      # not converted adjusted p value
# if p_adj_dw < 0.5;
#     p_adj_dw = p_adj_dw  * 2;                           # convert to p-values
# else
#     p_adj_dw = (1-p_adj_dw)  * 2;                       # convert to pvalues used by se="nid
# end

# # start using adj_p_pk function that does this
# # [pwtw,wtsw] = quantreg(wyoi',wp_wt_ta_rcew,.5,1,1500);       # quantile regression air temp, rcew
# # rstw = wp_wt_ta_rcew' - polyval(pwtw,wyoi);           # residuals for Ta trend
# # r1tw = autocorr(rstw,1); ac1tw = abs(r1tw(2));        # get serial corr. of resids.
# # netw = length(rstw).*((1-ac1tw)/(1+ac1tw));           # effective sample size air temp whole watershed
# # t_orig_tw = pwtw(1)/wtsw.pse(1);                       # slope divided by standard error gives original t value
# # # p_orit_tw = 2*(1-tcdf(abs(t_orig_tw),length(rstw)-2))  # original p-value
# # t_adj_tw = t_orig_tw*(sqrt(netw-2))/(sqrt(length(rstw)-2)); # adjusted t value for effective sample size and df
# # p_adj_tw = tcdf(t_adj_tw,netw-2);                      # not converted adjusted p value
# # if p_adj_tw < 0.5
# #     p_adj_tw = p_adj_tw  * 2;                          # convert to p-values
# # else
# #     p_adj_tw = (1-p_adj_tw)  * 2;                      # convert to pvalues used by se="nid
# # end

# [pwtw,wtsw,p_adj_tw] = adj_p_pk(wyoi',wp_wt_ta_rcew,.5,1500);
# [pudw,udsw,p_adj_du] = adj_p_pk(wyoi',wp_wt_dp_ut,.5,1500);
# [putw,utsw,p_adj_tu] = adj_p_pk(wyoi',wp_wt_ta_ut,.5,1500);
# [pmdw,mdsw,p_adj_dm] = adj_p_pk(wyoi',wp_wt_dp_mt,.5,1500);
# [pmtw,mtsw,p_adj_tm] = adj_p_pk(wyoi',wp_wt_ta_mt,.5,1500);
# [pldw,ldsw,p_adj_dl] = adj_p_pk(wyoi',wp_wt_dp_lt,.5,1500);
# [pltw,ltsw,p_adj_tl] = adj_p_pk(wyoi',wp_wt_ta_lt,.5,1500);


# ###########################################################################

# ## plot TA AND DEW PT TEMP MEANS FROM elevation bands for entire water year
# ###########################################################################
#   [le,bo,wi,he] = subplot_spacing(4,2,.06,.05,.01,.09,.01,.01);
# # [le,bo,wi,he] = subplot_spacing(prows,pcols,bot_brd,top_brd,rit_brd,lef_brd,v_space,h_space)
# scrsz = get(groot,'ScreenSize');

# fs = 14; # axis font size
# afs = 15; # annotation font sizes
# tfs = 16; # title font size
# ylfs = 16; # ylabel font size
# yl = [-6 9];
# xl = [1983 2015];
# xt = 1985:5:2010;
# yt = -4:2:8;
# ms = 10; # marker size

# close(f);
# f = figure('Position',[.1 .1 scrsz(3)*.4 scrsz(4)*.6]);

# # WHOLE CATCHMENT DEW POINT TEMP. #########################################
# subplot('position',[le(1) bo(1) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,wp_wt_dp_rcew,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# title('Winter Precipitation-Weighted Dew Point Temperature','fontsize',tfs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'Whole Catchment';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pwdw,wyoi),'-b','linewidth',2);
# plot(wyoi,wdsw.yfitci(:,1),'-b')
# plot(wyoi,wdsw.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pwdw(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_dw,3))],'FontSize',afs,'color','b')

# # WHOLE CATCHMENT AIR TEMP. ###############################################
# subplot('position',[le(2) bo(2) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# hold on
# plot(wyoi,wp_wt_ta_rcew,'or','linewidth',2,'markersize',ms)
# title('Winter Precipitation-Weighted Air Temperature','fontsize',tfs)
# plot(wyoi,polyval(pwtw,wyoi),'-r','linewidth',2);
# plot(wyoi,wtsw.yfitci(:,1),'-r')
# plot(wyoi,wtsw.yfitci(:,2),'-r')
# text(1985,7,['slope = ' num2str(round(pwtw(1),2))],'FontSize',afs,'color','r')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_tw,3))],'FontSize',afs,'color','r')

# # UPPER ELEV. DEW POINT TEMP. #############################################
# subplot('position',[le(3) bo(3) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on 
# plot(wyoi,wp_wt_dp_ut,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'High Elevation';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pudw,wyoi),'-b','linewidth',2);
# plot(wyoi,udsw.yfitci(:,1),'-b')
# plot(wyoi,udsw.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pudw(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_du,3))],'FontSize',afs,'color','b')

# # UPPER ELEV. AIR TEMP. ###################################################
# subplot('position',[le(4) bo(4) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,wp_wt_ta_ut,'or','linewidth',2,'markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# plot(wyoi,polyval(putw,wyoi),'-r','linewidth',2);
# plot(wyoi,utsw.yfitci(:,1),'-r')
# plot(wyoi,utsw.yfitci(:,2),'-r')
# text(1985,7,['slope = ' num2str(round(putw(1),2))],'FontSize',afs,'color','r')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_tu,3))],'FontSize',afs,'color','r')

# # MID ELEV. DEW POINT TEMP. #############################################
# subplot('position',[le(5) bo(5) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,wp_wt_dp_mt,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'Mid. Elevation';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pmdw,wyoi),'-b','linewidth',2);
# plot(wyoi,mdsw.yfitci(:,1),'-b')
# plot(wyoi,mdsw.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pmdw(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_dm,3))],'FontSize',afs,'color','b')

# # MID ELEV. AIR TEMP. ###################################################
# subplot('position',[le(6) bo(6) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,wp_wt_ta_mt,'or','linewidth',2,'markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# plot(wyoi,polyval(pmtw,wyoi),'-r','linewidth',2);
# plot(wyoi,mtsw.yfitci(:,1),'-r')
# plot(wyoi,mtsw.yfitci(:,2),'-r')
# text(1985,7,['slope = ' num2str(round(pmtw(1),2))],'FontSize',afs,'color','r')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_tm,3))],'FontSize',afs,'color','r')

# # LOW ELEV. DEW POINT TEMP. #############################################
# subplot('position',[le(7) bo(7) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,wp_wt_dp_lt,'ob','markerfacecolor','b','markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'xlim',xl,'xtick',xt,'fontsize',fs)
# ylab = sprintf('Temperature (#cC)', char(176));
# ylabel({'Low Elevation';ylab},'fontsize',ylfs)
# plot(wyoi,polyval(pldw,wyoi),'-b','linewidth',2);
# plot(wyoi,ldsw.yfitci(:,1),'-b')
# plot(wyoi,ldsw.yfitci(:,2),'-b')
# text(1985,7,['slope = ' num2str(round(pldw(1),2))],'FontSize',afs,'color','b')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_dl,3))],'FontSize',afs,'color','b')

# # LOW ELEV. AIR TEMP. ###################################################
# subplot('position',[le(8) bo(8) wi he]);
# plot(xl,[0 0],'linestyle','--','linewidth',3,'color',[.5 .5 .5])
# hold on
# plot(wyoi,wp_wt_ta_lt,'or','linewidth',2,'markersize',ms)
# set(gca,'ylim',yl,'ytick',yt,'yticklabel',[],'xlim',xl,'xtick',xt,'fontsize',fs)
# plot(wyoi,polyval(pltw,wyoi),'-r','linewidth',2);
# plot(wyoi,ltsw.yfitci(:,1),'-r')
# plot(wyoi,ltsw.yfitci(:,2),'-r')
# text(1985,7,['slope = ' num2str(round(pltw(1),2))],'FontSize',afs,'color','r')
# text(1985,5.75,['p-value = ' num2str(round(p_adj_tl,3))],'FontSize',afs,'color','r')

# ## figure saver
# set(gcf, 'Color', 'w','PaperPositionMode', 'auto');
# print -djpeg -loose -r300 '~/Documents/papers/precip_dist_30yr_chng/figs/ppt_wt_ta_td_elev_trends_winter.jpg'

# ###########################################################################

# ## GET ELEVATION OF PHASE CHANGE FOR EACH YEAR. (AWESOME HIST/MAP FIGURE)
# ###########################################################################
# clear all; close all; clc
# [dem,xvec,yvec] = agrid_inv('~/rcew_met_dist/terrain/rcew_10m.txt'); # bring in dem
# dem(dem==0) = nan;
# wyoi = 1984:2014;
# rain_mixed_elev = nan(length(wyoi),1);
# snow_mixed_elev = nan(length(wyoi),1);

# for y = 1:length(wyoi);
#     #     load(['wy' num2str(wyoi(y)) 'w_pptwt_ta_td.mat'],'wd'); # for winter dew point temp
#     #     wd = wd.*mskn;
#     #     upc = wd <= -0.45 & wd >= -0.55; # index of locations of upper rstz boundary
#     #     lpc = wd <= 0.55 & wd >= 0.45; # index of lower rstz boundary
#     load(['wy' num2str(wyoi(y)) '_pptwt_ta_td.mat'],'d'); # for water year dew point temp
#     d(isnan(dem)) = nan;
#     upc = d <= -0.45 & d >= -0.55; # index of locations of upper rstz boundary
#     lpc = d <=  0.55 & d >=  0.45; # index of lower rstz boundary

#     upce = dem(upc);             # if it does occur on the watershed
#     snow_mixed_elev(y) = mean(upce); # assign it the mean elevation of those pixels
#     #     figure(1); clf; f=16;
#     #     subplot(1,2,1)
#     #     cnts = hist(upce,30);
#     #     hist(upce,30)
#     #     hold on
#     #     plot([snow_mixed_elev(y) snow_mixed_elev(y)],[0,max(cnts)],'-r','linewidth',2)
#     #     title(['Snow line for ' num2str(wyoi(y))])
#     #     set(gca,'fontsize',f)
#     #     subplot(1,2,2)
#     #     iscet(upc)
#     #     set(gca,'fontsize',f)
#     #     pause
#     lpce = dem(lpc);
#     rain_mixed_elev(y) = mean(lpce);
#     #     figure(2); clf;
#     #     subplot(1,2,1)
#     #     cnts = hist(lpce,30);
#     #     hist(lpce,30)
#     #     ylabel('counts')
#     #     xlabel('elevation (m)')
#     #     set(gca,'fontsize',f)
#     #     hold on
#     #     plot([rain_mixed_elev(y) rain_mixed_elev(y)],[0,max(cnts)],'-r','linewidth',2)
#     #     title(['Rain line for ' num2str(wyoi(y))])
#     #     subplot(1,2,2)
#     #     iscet(lpc)
#     #     set(gca,'fontsize',f)
#     #     pause
# end

    
# ###########################################################################

# ## PLOT ELEVATION OF PHASE CHANGE OVER TIME
# ###########################################################################
# scrsz = get(groot,'ScreenSize');
# # close(f);
# f = figure('Position',[.1 .1 scrsz(3)*.4 scrsz(4)*.25]);
# sp = plot(wyoi,snow_mixed_elev,'ob','MarkerFaceColor','b','markersize',12);
# hold on
# [psnw,ssnw,pval_snowline] = adj_p_pk(wyoi(isfinite(snow_mixed_elev))',snow_mixed_elev(isfinite(snow_mixed_elev)),.5,500);
# sm = plot(wyoi,polyval(psnw,wyoi),'-b','linewidth',2);
# text(1984,2200,['slope = ' num2str(psnw(1),'#2.1f')],'FontSize',18,'color','b')
# text(1984,2100,['pval = ' num2str(round(pval_snowline,3))],'FontSize',18,'color','b')
# rp = plot(wyoi,rain_mixed_elev,'ok','MarkerFaceColor','k','markersize',12);
# [prn,srn,pval_rainline] = adj_p_pk(wyoi(isfinite(rain_mixed_elev))',rain_mixed_elev(isfinite(rain_mixed_elev)),.5,500);
# rm = plot(wyoi,polyval(prn,wyoi),'-k','linewidth',2);
# text(1984,1100,['slope = ' num2str(prn(1),'#2.1f')],'FontSize',18,'color','k')
# text(1984,1000,['pval = ' num2str(round(pval_rainline,3))],'FontSize',18,'color','k')
# title('Changes in Rain/Snow Transition Elevation 1984-2014')
# ylabel('Elevation (m)')
# xlabel('Water Year')
# set(gca,'fontsize',18,'ylim',[850 2500],'xlim',[1983 2015])
# legend([sp,sm,rp,rm],'Snow Line Elevation','Snow Line Model','Rain Line Elevation','Rain Line Model','location','southeast')

# ## figure saver
# set(gcf, 'Color', 'w','PaperPositionMode', 'auto');
# print -djpeg -loose -r300 '~/Documents/papers/precip_dist_30yr_chng/figs/rstz_elev_trend.jpg'

    
# ###########################################################################

# ## PLOT MAP PHASE CHANGE OVER TIME
# ###########################################################################
# tt = polyval(psnw,wyoi);
# se84 = tt(1);
# se14 = tt(end);
# tt = polyval(prn,wyoi);
# re84 = tt(1);
# re14 = tt(end);
# clear tt
# mine = min(dem(:));
# maxe = max(dem(:));
# t = 20;
# pdem = dem; # plotting dem
# pdem(isnan(pdem)) = min(dem(isfinite(dem)))-25;


# # get locations of snowcourses
# # scx =[517892 515864 521613 520055 516719 515687 515042 514041];
# # scy = [4770341 4771970 4769718 4768117 4767777 4768520 4769342 4769438];
# # scxi = scx;
# # scyi = scy;
# # for i = 1:length(scx);
# #     scyi(i) = searchclosest1(fliplr(yvec),scy(i));
# #     scxi(i) = searchclosest1(xvec,scx(i));
# # end
# ##
#   [le,bo,wi,he] = subplot_spacing(1,2,.06,.05,.01,.01,.01,.01);
# # [le,bo,wi,he] = subplot_spacing(prows,pcols,bot_brd,top_brd,rit_brd,lef_brd,v_space,h_space)

# scrsz = get(groot,'ScreenSize');
# close(f);
# f = figure('Position',[.1 .1 scrsz(3)*.28 scrsz(4)*.4]);
# sp(1) = subplot('position',[le(1),bo(1),wi,he]);
# cm1 = demcmap([mine maxe],64);
# iscet(pdem)
# colormap(cm1)
# newmap1 = colormap(sp(1)); 
# cscale = linspace(mine,maxe,size(newmap1,1)); # color scale
# ttl = searchclosest1(cscale,re84); # find low index
# ttu = searchclosest1(cscale,se84); # find up index
# ttb = searchclosest1(cscale,0);    # background
# newmap1(ttl:ttu,:) = repmat([.8 .8 .8],ttu-ttl+1,1); # gray
# newmap1(ttb,:) = repmat([1 1 1],1,1); # white background
# colormap(sp(1),newmap1);                #activate it
# title('Rain/Snow Transition Zone 1984','fontsize',t)
# set(gca,'xtick',[],'ytick',[],'fontsize',15)
# # hold on
# # plot(scxi,scyi,'ok','markerfacecolor','w','markersize',10)
# # hold off

# sp(2) = subplot('position',[le(2),bo(2),wi,he]);
# colormap('default')
# cm2 = demcmap([mine maxe],64);
# iscet(pdem)
# colormap(cm2)
# newmap2 = colormap(sp(2)); 
# cscale = linspace(mine,maxe,size(newmap2,1)); # color scale
# ttl = searchclosest1(cscale,re14); # find low index
# ttu = searchclosest1(cscale,se14); # find up index
# ttb = searchclosest1(cscale,0);    # background
# newmap2(ttl:ttu,:) = repmat([.8 .8 .8],ttu-ttl+1,1); # gray
# newmap2(ttb,:) = repmat([1 1 1],1,1); # white background
# colormap(sp(2),newmap2);                #activate it
# title('Rain/Snow Transition Zone 2014','fontsize',t)
# set(gca,'xtick',[],'ytick',[],'fontsize',15)
# # hold on
# # plot(scxi,scyi,'ok','markerfacecolor','w','markersize',10)
# # hold off
# ## figure saver
# set(gcf, 'Color', 'w','PaperPositionMode', 'auto');
# print -djpeg -loose -r300 '~/Documents/papers/precip_dist_30yr_chng/figs/rstz_map_84to14.jpg'

# ##
# tt = dem>se84;
# sarea1984 = sum(tt(:))/sum(isfinite(dem(:)));
# tt = dem>se14;
# sarea2014 = sum(tt(:))/sum(isfinite(dem(:)));
    
# tt = dem<se84 & dem>re84;
# marea1984 = sum(tt(:))/sum(isfinite(dem(:)));
# tt = dem<se14 & dem>re14;
# marea2014 = sum(tt(:))/sum(isfinite(dem(:)));
    
# ## wy1999 precip weighted winter dew point temperature figure
# load wy1999w_pptwt_ta_td.mat
# scrsz = get(groot,'ScreenSize');
# t = 25;
# l = 24;
# fs = 20;
# ms = 11;

# f = figure(19); clf;
# set(f,'Position',[1 1 scrsz(3)/3 scrsz(4).*.8]);
# iscet(wd)
# set(gca,'xtick',[],'ytick',[],'fontsize',fs)
# title('     Winter Precip. Wt. Dew Point Temp. 1999','fontsize',t)
