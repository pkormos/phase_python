'''
Created on Nov 8, 2016
Script mod'd from matlab script to run dk to  distirbute early relative humidity at rcew
@author: pkormos
'''

import netCDF4 as nc
import numpy as np
from pandas import DataFrame, offsets
import os
# import subprocess as sp 
# import matplotlib.pyplot as plt

wyoi = np.arange(1999, 2000)  # water years of interest
outdir = '/Volumes/megadozer/CZO/working_dir/'   # define input dir
# outdir_out = '/Volumes/megadozer/CZO/rh_corrected/' # define output dir
os.chdir(outdir) # goto directory

# bring in rh dataframe so we can put the station data in with the 
rnc = '/Volumes/megadozer/CZO/nc_working_files/rcew_rh_pk.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r')            # read rh timeseries ncdf file
tt = rn.variables['time']
time = nc.num2date(tt[:], tt.units)
rvar = rn.variables["relative_humidity"]
rh_st = rn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(rvar)[1])]
r_list = [rh_st.getncattr(tt_list[k]) for k in range(np.shape(rvar)[1])] # sorted rh station list
rh = DataFrame(np.ma.filled(rvar[:,0:21],np.nan),index=time,columns=r_list[0:21]) # make pandas dataset
xyzvar = rn.variables["easting_northing_elev"]
stnxyz = DataFrame(xyzvar[:],index=r_list,columns=["x","y","z"]) # make pandas dataset
rn.close()
rh.index = rh.index-offsets.Hour(-7)    # fix time back to local time


## loop through water years to post process net cdf files from dk
for y in wyoi: # for each year
	# get station rh data
    dat_bloc  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y].dropna(axis=1,how='all')
    
    # take out stations D03 and RMSP for entire year 1984-1998
    if np.sum(np.arange(1984,1999)==y)>0:
        del dat_bloc['rc.usc-d03']
        del dat_bloc['rc.tg.rme-rmsp']
    
    # WY1999, rmsp comes in for some times, D03 needs to come out for the whole year. see time frames from below
    if y==1999:
        del dat_bloc['rc.usc-d03']
        dat_bloc.ix['1998-10-1':'1998-10-27 10:00',['rc.tg.rme-rmsp']] = np.nan
        dat_bloc.ix['1999-07-8 06:00':'1999-07-14 13:00',['rc.tg.rme-rmsp']] = np.nan
        dat_bloc.ix['1999-09-17 03:00':'1999-10-1 00:00',['rc.tg.rme-rmsp']] = np.nan
        dat_bloc = dat_bloc.astype(float)
        
    # WY2000, 10-01-99 00:00 to 10-06-99 9:00, take out rmsp. It's just copied from 176.
    if y == 2000:
        del dat_bloc['rc.usc-d03']
        dat_bloc.ix['1999-10-1':'1999-10-6 9:00',['rc.tg.rme-rmsp']] = np.nan
        
    # take out D03 for entire year 2001:2014
    if np.sum(np.arange(2001,2015)==y)>0:
        del dat_bloc['rc.usc-d03']
        
    ncname = "rh_wy%s.nc" % y           # define netcdf file name
    dat_bloc = dat_bloc.round(2)      	# get data to write in the correct format
    dat_bloc = dat_bloc.fillna(-9999) 	# replace NaN with _FillValue

    os.rename("dk_out_%s.nc"%y, ncname) # rename netcdf file
    
    rdn = nc.Dataset(ncname, 'r+')       # open distributed rh ncdf file
    
    ## general fixing of dk .nc file
    rdn.renameVariable('variable','relative_humidity')   # rename variable
    rhvar = rdn.variables['relative_humidity']
    rhvar.units = 'decimal_percent'
    rhvar.long_name = 'relative humidity from 0 to 1'
##     rhvar.grid_mapping_name = "universal_transverse_mercator"
##     rhvar.coordinates = "lat lon" 
      
    xvar = rdn.variables["x"]
    xvar.long_name = 'easting (meters)'
    xvar.Coordinate_System = 'UTM_NAD83_Zone11'
      
    yvar = rdn.variables["y"]
    yvar.long_name = 'northing (meters)'
    yvar.Coordinate_System = 'UTM_NAD83_Zone11'
      
    tvar = rdn.variables["time"]
    tvar.units = 'hours since %s-10-01 00:00:00 -7:00' %  y
       
      
    ### write stations variable
    sdim = rdn.createDimension('stations',dat_bloc.shape[1])
    svar = rdn.createVariable('stations','i4','stations')
    svar[:] = np.arange(0,dat_bloc.shape[1])
#     svar = rdn.variables['stations']
    svar.setncatts({'station_%s'%k: dat_bloc.columns[k] for k in range(np.size(dat_bloc.columns))}) # add station names
    svar.long_name = "names of stations used to distribute measured relative humidity data"
    
#     ### write raw data    
    tdim = rdn.dimensions["time" ]
    dvar = rdn.createVariable('station_data','f',('time','stations'),fill_value=-9999)
#     dvar = rdn.variables['station_data']
    dvar[:] = dat_bloc.as_matrix()
    dvar.long_name = "weather station time series data used to distribute relative humidity over RCEW"

    ### write easting_northing variable
    rdn.createDimension("easting_northing",2)       # create station coord. dimension 2 for x y
    envar = rdn.createVariable("easting_northing",'i4',"easting_northing") # make stn coords variable
    #     envar = rdn.variables["easting_northing"]
    envar[:] = range(2)                                     # give it integers
    envar.long_name = "easting and northing of weather station locations" 
    envar.units = "meters" 

    ### write station coordinates
    stnvar = rdn.createVariable('station_coordinates','f',("stations","easting_northing"))
    for c in range(np.size(dat_bloc.columns)):
        stnvar[c,:] = stnxyz.ix[list(stnxyz.index.values).index(dat_bloc.columns[c]),0:2].as_matrix()
    stnvar.units = "easting_meters, northing_meters"
    rdn.close()
    




