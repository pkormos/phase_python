#!/usr/local/bin/python2.7
# encoding: utf-8
'''
bring_in_ml_files -- this program will take the corrected weather data in the .mat files and pull it into netcdf formats

bring_in_ml_files is a script to bring in .mat files. relative humidity

@author:     patrick kormos
@contact:    patrick.kormos@ars.usda.gov
@deffield    updated: Updated
'''

# import sys
# import os
import netCDF4 as nc
import numpy as np
# import time
# import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import h5py

pmat = h5py.File('/Users/pkormos/rcew_met_dist/met/latest_data/rcew_ppt_pk.mat')
sdate = datetime(1961,12,31,23)       # start date time
edate = datetime(2015,1,1,0)  # end date time
hcnt = ((edate-sdate).days)*24+1 # number of hours from start time to stop time
ptime = [sdate + timedelta(hours=n) for n in range(hcnt)]

p_list = np.array(['rc-047', 'rc-055', 'rc-059', 'rc-074', 'rc-075', 'rc-076', 'rc-078', 'rc-083',
    'rc-088', 'rc-095b', 'rc-097', 'rc-106', 'rc-114', 'rc-119', 'rc-128', 'rc.fl-057',
    'rc.lsc-127', 'rc.mc-043041', 'rc.mc-053', 'rc.mc-054', 'rc.mc-061', 'rc.mc-072',
    'rc.ng-098c', 'rc.ng-108', 'rc.sc-012', 'rc.sc-015', 'rc.sc-023', 'rc.sc-024',
    'rc.sc-031','rc.sc-045', 'rc.sc.mp-033', 'rc.sum-049', 'rc.tg-116c','rc.tg-126',
    'rc.tg-145', 'rc.tg-147', 'rc.tg-155', 'rc.tg-156', 'rc.tg-165', 'rc.tg-167',
    'rc.tg-174_ppt', 'rc.tg.dc-144', 'rc.tg.dc-154', 'rc.tg.dc-163', 'rc.tg.dc-163',
    'rc.tg.dc.jd-124', 'rc.tg.dc.jd-124b', 'rc.tg.dc.jd-125', 'rc.tg.rme-166b',
    'rc.tg.rme-176_ppt', 'rc.tg.rme-rmsp', 'rc.tg.rmw-166x94','rc.usc-138031', 
    'rc.usc-138044', 'rc.usc-d03','rc.usc-j10', 'rc.usc-l21'])

p_all = pmat.get("ppt_corrected")
# set some plot values

pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
pn = nc.Dataset(pnc, 'w',format="NETCDF4")            # create precip ncdf file
time = pn.createDimension("time", None)               # create time dimension
stns = pn.createDimension("stations",np.size(p_list)) # create stations dimension
pvar = pn.createVariable("precipitation","f4",("time","stations",))
pvar[:] = np.transpose(p_all)
pn.close








