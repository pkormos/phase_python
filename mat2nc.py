'''
bring_in_ml_files -- this program will take the corrected weather data in the .mat files and pull it into netcdf formats

does precip station data
rh station data (which is then modified and resaved)
precipitation totals

@author:     patrick kormos
@contact:    patrick.kormos@ars.usda.gov
'''

import netCDF4 as nc
import numpy as np
from pandas import DataFrame, date_range
import h5py


# #### THIS CODE CHUNK WILL CONVERT PRECIPITATION STATION DATA .MAT FILE TO .NC FILE ####
# pmat = h5py.File('/Users/pkormos/rcew_met_dist/met/latest_data/rcew_ppt_pk.mat') # connect to .mat database
# p_list = np.array(['rc-047', 'rc-055', 'rc-059', 'rc-074', 'rc-075', 'rc-076', 'rc-078', 'rc-083', # manually bring in column headers
#     'rc-088', 'rc-095b', 'rc-097', 'rc-106', 'rc-114', 'rc-119', 'rc-128', 'rc.fl-057',
#     'rc.lsc-127', 'rc.mc-043041', 'rc.mc-053', 'rc.mc-054', 'rc.mc-061', 'rc.mc-072',
#     'rc.ng-098c', 'rc.ng-108', 'rc.sc-012', 'rc.sc-015', 'rc.sc-023', 'rc.sc-024',
#     'rc.sc-031','rc.sc-045', 'rc.sc.mp-033', 'rc.sum-049', 'rc.tg-116c','rc.tg-126',
#     'rc.tg-145', 'rc.tg-147', 'rc.tg-155', 'rc.tg-156', 'rc.tg-165', 'rc.tg-167',
#     'rc.tg-174', 'rc.tg.dc-144', 'rc.tg.dc-154', 'rc.tg.dc-163', 'rc.tg.dc-163',
#     'rc.tg.dc.jd-124', 'rc.tg.dc.jd-124b', 'rc.tg.dc.jd-125', 'rc.tg.rme-166b',
#     'rc.tg.rme-176', 'rc.tg.rme-rmsp', 'rc.tg.rmw-166x94','rc.usc-138031', 
#     'rc.usc-138044', 'rc.usc-d03','rc.usc-j10', 'rc.usc-l21'])
# ppt = DataFrame(np.transpose(pmat.get("ppt_corrected")), index=date_range('1962-1-1', '2014-12-31 23:00', freq='H'), columns = p_list) # make pandas dataset
# ppt = ppt.replace('NaN',-9999)
#  
#  
# # create the ppt netcdf file.
# pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
# pn = nc.Dataset(pnc, 'w',format="NETCDF4",clobber=True)            # create precip ncdf file
# time = pn.createDimension("time", None)               # create time dimension
# stns = pn.createDimension("stations",np.size(p_list)) # create stations dimension
# tvar = pn.createVariable("time","int",("time"))
# tvar[:] = np.arange(0,np.shape(ppt)[0])
# tvar.units = "hours since 1962-01-01 00:00:00 -7:00"
# tvar.long_name = "time"
# stnn = pn.createVariable("stations","int",("stations"))
# for i in np.arange(0,np.shape(p_list)[0]):
#     setattr(pn.variables['stations'], 'station_%s'%i,p_list[i])
# stnn[:] = np.arange(0,np.shape(p_list)[0])
# pvar = pn.createVariable("precipitation","f4",("time","stations",),fill_value=-9999)
# pvar.long_name = "corrected precipitation depth per hour time step"
# pvar.units = "mm"
# pvar[:] = ppt.as_matrix()
# pn.close()

####################################################################################################################################

# # rh has been modified by plot_and_correct_humitiyxxx.py. if you uncomment and rerun this code, you must rerun these scripts.

#### THIS CODE CHUNK WILL CONVERT RELATIVE HUMIDITY MAT FILE TO A .NC FILE ####
rmat = h5py.File('/Users/pkormos/rcew_met_dist/met/latest_data/rcew_rh_pk.mat') # connect to .mat database
r_list = np.array(['rc-076','rc-095b','rc-128','rc.lsc-127','rc.sc-012','rc.sc-031',
    'rc.tg-145','rc.tg-167','rc.tg-167b','rc.tg-174','rc.tg.dc-144',
    'rc.tg.dc-163',
    'rc.tg.dc.jd-124',
    'rc.tg.dc.jd-124b',
    'rc.tg.dc.jd-125',
    'rc.tg.rme-166b',
    'rc.tg.rme-176',
    'rc.tg.rme-rmsp',
    'rc.usc-d03',
    'rc.usc-j10',
    'rc.usc-l21',
    'rc-076_tf',
    'rc-128_tf',
    'rc.lsc-127_tf',
    'rc.tg.rme-176_tf'])
rh = DataFrame(np.transpose(rmat.get("rh")), index=date_range('18-Jun-1981 11:00:00', '01-Oct-2014', freq='H'), columns = r_list) # make pandas dataset
rh = rh.replace('NaN',-9999)
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_rh_pk.nc' # netcdf file name
# rn = nc.Dataset(rnc, 'w',format="NETCDF4",clobber=True)            # create rh ncdf file
# time = rn.createDimension("time", None)               # create time dimension
# stns = rn.createDimension("stations",np.size(r_list)) # create stations dimension
# tvar = rn.createVariable("time","int",("time"))
# tvar[:] = np.arange(0,np.shape(rh)[0])
# tvar.units = "hours since 1981-06-18 11:00:00 -7:00"
# tvar.long_name = "time"
# stnn = rn.createVariable("stations","int",("stations"))
# stnn[:] = np.arange(0,np.shape(r_list)[0])
# for i in np.arange(0,np.shape(r_list)[0]):
#     setattr(rn.variables['stations'], 'station_%s'%i,r_list[i])
# rvar = rn.createVariable("relative_humidity","f4",("time","stations",),fill_value=-9999)
# rvar.long_name = "relative humidity"
# rvar.units = "decimal percent"
# rvar[:] = rh.as_matrix()
# rn.close()
rn = nc.Dataset(rnc, 'r+')  # open rh ncdf file
rn_rh = rn.variables['relative_humidity']
rn_rh[:,np.squeeze(np.argwhere(r_list=='rc-128'))] = rh['rc-128'].as_matrix()
rn.close()

 
# # append station xyz onto rh file
# rmat = h5py.File('/Users/pkormos/rcew_met_dist/met/latest_data/rcew_rh_pk.mat') # connect to .mat database
coords = rmat.get("rh_stn_xyz")
# rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_rh_pk.nc' # netcdf file name
# rn = nc.Dataset(rnc, 'r+',format="NETCDF4")            # create precip ncdf file
xyzdim = rn.createDimension("easting_northing_elev",3) # create stations dimension
xyzvar = rn.createVariable("easting_northing_elev","float",("stations","easting_northing_elev"))
xyzvar[:] = np.transpose(coords[:])
xyzvar.station_coordinate_0 = "easting (meters)"
xyzvar.station_coordinate_1 = "northing (meters)"
xyzvar.Coordinate_System = "UTM_NAD83_Zone11"
rn.close()


# ####################################################################################################################################
# ####### CODE TO CONVERT WATER YEAR TOTAL PRECIPTIATION FROM .MAT FILE TO NETCDF FILE ###############################################
# ####################################################################################################################################
# wyoi = range(1984,2015)
# ptmat = h5py.File('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/ppt_wytot.mat') # connect to .mat database
# ppt = np.transpose(ptmat.get("ppt_wytot"),(0,2,1))


# ppt_file = "/Volumes/LaCie/precip_cf/precip_wy1984.nc"
# p_ncid = nc.Dataset(ppt_file,'r')                      # open ncdf file with correct x and y information

# ppt_tot_file = nc.Dataset("/Volumes/megadozer/CZO/data_products/rcew_ppt_wytot.nc",'w')
# ppt_tot_file.createDimension('time',size=None) # make an unlimited dimension

# ydim = p_ncid.dimensions['y']
# ppt_tot_file.createDimension(ydim.name,len(ydim))
# xdim = p_ncid.dimensions['x']
# ppt_tot_file.createDimension(xdim.name,len(xdim))


# wyvar = ppt_tot_file.createVariable("time","i","time")
# wyvar[:] = wyoi

# px = p_ncid.variables['x']
# x  = ppt_tot_file.createVariable(px.name,px.datatype,px.dimensions) # make same xvar
# x[:] = px[:] # put x coords in there

# py = p_ncid.variables['y']
# y  = ppt_tot_file.createVariable(py.name,py.datatype,py.dimensions) # make same xvar
# y[:] = py[:] # put x coords in there

# wytot_var = ppt_tot_file.createVariable('ppt_wytot','f4',('time','y','x')) # create vp variable
# wytot_var[:] = ppt[:,:,:]

# p_ncid.close()
# ppt_tot_file.close()
  
#  # could improve this sometime to make it more netcdf complient

####################################################################################################################################
####### CODE TO CONVERT WINTER WATER YEAR TOTAL PRECIPTIATION FROM .MAT FILE TO NETCDF FILE ########################################
####################################################################################################################################
# wyoi = range(1984,2015)
# ptmat = h5py.File('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/ppt_wtot.mat') # connect to .mat database
# ppt = np.transpose(ptmat.get("wppt_total"),(0,2,1))
# 
# 
# ppt_file = "/Volumes/LaCie/precip_cf/precip_wy1984.nc"
# p_ncid = nc.Dataset(ppt_file,'r')                      # open ncdf file with correct x and y information
# 
# ppt_tot_file = nc.Dataset("/Volumes/megadozer/CZO/data_products/rcew_ppt_winter_tot.nc",'w')
# ppt_tot_file.createDimension('time',size=None) # make an unlimited dimension
# 
# ydim = p_ncid.dimensions['y']
# ppt_tot_file.createDimension(ydim.name,len(ydim))
# xdim = p_ncid.dimensions['x']
# ppt_tot_file.createDimension(xdim.name,len(xdim))
# 
# 
# wyvar = ppt_tot_file.createVariable("time","i","time")
# wyvar[:] = wyoi
# 
# px = p_ncid.variables['x']
# x  = ppt_tot_file.createVariable(px.name,px.datatype,px.dimensions) # make same xvar
# x[:] = px[:] # put x coords in there
# 
# py = p_ncid.variables['y']
# y  = ppt_tot_file.createVariable(py.name,py.datatype,py.dimensions) # make same xvar
# y[:] = py[:] # put x coords in there
# 
# wytot_var = ppt_tot_file.createVariable('ppt_winter_tot','f4',('time','y','x')) # create vp variable
# wytot_var[:] = ppt[:,:,:]
# 
# p_ncid.close()
# ppt_tot_file.close()
  
 # could improve this sometime to make it more netcdf complient



