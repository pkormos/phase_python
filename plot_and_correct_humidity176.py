'''
Created on Nov 7, 2016
script to plot and correct relative humidity data from rme176 based on the plots from 
humidity_analysis.py. and time series plot from this script. also updates the netCDF file
now located on bunnyhill. plots up before and after histograms.

@author: pkormos
'''

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series, date_range

def rhstretch(data,old_low,new_low,old_high):
    slp = (1-new_low)/(old_high-old_low)
    itc = new_low - old_low * slp
    return(data*slp+itc)

stn0 = 'rc.tg.rme-176'
wyoi = np.arange(1984, 2015)  # water years of interest

# bring in precipitation data
pnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_ppt_pk.nc' # netcdf file name
pn = nc.Dataset(pnc, 'r')            # create precip ncdf file
pvar = pn.variables["precipitation"]
p_st = pn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(pvar)[1])]
p_list = [p_st.getncattr(tt_list[k]) for k in range(np.shape(pvar)[1])] # sorted rh station list
ppt = Series(np.ma.filled(pvar[:,p_list.index(stn0)],np.nan).ravel(),index=date_range('1962-1-1', '2014-12-31 23:00', freq='H')) # make pandas dataset
pptd3 = Series(np.ma.filled(pvar[:,p_list.index('rc.usc-d03')],np.nan).ravel(),index=date_range('1962-1-1', '2014-12-31 23:00', freq='H')) # make pandas dataset
pn.close()

# # bring in relative humidity data
rnc = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rcew_rh_pk.nc' # netcdf file name
rn = nc.Dataset(rnc, 'r')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rh_st = rn.variables['stations']    # id for stations
tt_list = ["station_%s" % i for i in range(np.shape(rvar)[1])]
r_list = [rh_st.getncattr(tt_list[k]) for k in range(np.shape(rvar)[1])] # sorted rh station list
rh = Series(np.ma.filled(rvar[:,r_list.index(stn0)],np.nan).ravel(),index=date_range('18-Jun-1981 11:00:00', '01-Oct-2014', freq='H')) # make pandas dataset
rn.close()

# # plot up data to check it
# fig0 = plt.figure(num=10, figsize=(17,8.5))
# ax1 = fig0.add_subplot(2,1,1)
# ppt.plot()
# ax1.set_xlim('1983/10/1','2014/10/1')
# ax3 = fig0.add_subplot(2,1,2)
# rh.plot()
# ax3.set_xlim('1983/10/1','2014/10/1')
# ax3.hlines(y=1,xmin='1983/10/1',xmax='2014/10/1')

# # plot histograms for j10 rh during precip events
# fig1 = plt.figure(num=1, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig1.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
#     plt.show()
# fig1.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_176_before.tif', dpi=100) 

## plot original and corrected data
# plot original data
# fig2 = plt.figure(num=2, figsize=(14,8.5), dpi=100, facecolor='w')
# ax12 = fig2.add_axes([.05, .05, .9, .9]) 
# ppt['10/1/1983 0:00':'9/30/2006 23:00'].plot(style= 'g-', ax=ax12)
# ax2 = ax12.twinx()
# rh['10/1/1983 0:00':'9/30/2006 23:00'].plot(style = 'b-', ax=ax2) # plot time series before modification
# l0 = ax2.hlines(y=1,xmin='10/1/1982 0:00',xmax = '9/30/2006 23:00',color='k')


## correct first section of data
st1='1983-10-1 0:00'
nd1='1983-11-27 12:00'
strt  = 0.97  # what the upper rh does go up to
strb  = 0.08  # what the lower rh should drop to 
strbb = 0.28  # what the lower rh does drop to
# l0 = ax2.hlines(y=strb,xmin='10/1/1982 0:00',xmax = '9/30/2006 23:00',color='k')
# l1 = ax2.hlines(y=strt, xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l2 = ax2.hlines(y=strbb,xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd1,color='r',linewidth=2)
rh[st1:nd1] = rhstretch(rh[st1:nd1],strbb,strb,strt)
rh[st1:nd1] = rh[st1:nd1].clip_upper(1)
# l4 = rh[st1:nd1].plot(style='g-',ax=ax2)

## correct 2nd section of data
st1='1983-11-27 13:00'
nd1='1984-07-30 23:00'
strt  = 1  # what the upper rh does go up to
strbb = 0.27  # what the lower rh does drop to
# l2 = ax2.hlines(y=strbb,xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd1,color='r',linewidth=2)
rh[st1:nd1] = rhstretch(rh[st1:nd1],strbb,strb,strt)
rh[st1:nd1] = rh[st1:nd1].clip_upper(1)
# l4 = rh[st1:nd1].plot(style='c-',ax=ax2)

## correct 3nd section of data
st1='1984-07-31 0:00'
nd1='1984-11-10 0:00'
strt  = 0.98  # what the upper rh does go up to
strbb = 0.27  # what the lower rh does drop to
# l1 = ax2.hlines(y=strt, xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l2 = ax2.hlines(y=strbb,xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l3 = ax2.vlines(ymin=0,ymax=1,x=nd1,color='r',linewidth=2)
rh[st1:nd1] = rhstretch(rh[st1:nd1],strbb,strb,strt)
rh[st1:nd1] = rh[st1:nd1].clip_upper(1)
# l4 = rh[st1:nd1].plot(style='y-',ax=ax2)

## correct 4th section of data
st1='1984-12-11 0:00'
nd1='1985-11-11 0:00'
strt  = 0.96  # what the upper rh does go up to
strbb = 0.17  # what the lower rh does drop to
# l1 = ax2.hlines(y=strt, xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l2 = ax2.hlines(y=strbb,xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l3 = ax2.vlines(ymin=0,ymax=1,x=st1,color='r',linewidth=2)
# l4 = ax2.vlines(ymin=0,ymax=1,x=nd1,color='r',linewidth=2)
rh[st1:nd1] = rhstretch(rh[st1:nd1],strbb,strb,strt)
rh[st1:nd1] = rh[st1:nd1].clip_upper(1)
# l5 = rh[st1:nd1].plot(style='m-',ax=ax2)

## correct 5th section of data
st1='1985-11-11 1:00'
nd1='1986-02-23 0:00'
strt  = 0.96  # what the upper rh does go up to
strbb = 0.17  # what the lower rh does drop to
# l1 = ax2.hlines(y=strt, xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l2 = ax2.hlines(y=strbb,xmin=st1, xmax=nd1,color='r', linewidth=2) # plot line showing area to be changed
# l3 = ax2.vlines(ymin=0,ymax=1,x=st1,color='r',linewidth=2)
# l4 = ax2.vlines(ymin=0,ymax=1,x=nd1,color='r',linewidth=2)
rh[st1:nd1] = rhstretch(rh[st1:nd1],strbb,strb,strt)
rh[st1:nd1] = rh[st1:nd1].clip_upper(1)
# l5 = rh[st1:nd1].plot(style='m-',ax=ax2)


# fig2.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_j10_before_after.tif', dpi=100) 

# fig3 = plt.figure(num=3, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[0:14]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0d = pptd3['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0[ppt0.isnull()] = ppt0d[ppt0.isnull()]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig3.add_subplot(7,2,y-1983)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
#     #     plt.show()
# fig3.savefig('/Users/pkormos/rcew_met_dist/snow_data/phase_analysis/phase_python/rh_hist_176_after.tif', dpi=100) 

# ###############3###############3###############3###############3###############3###############3
# fig4 = plt.figure(num=4, figsize=(8.5,14), dpi=100, facecolor='w')
# for y in np.nditer(wyoi[14:28]):    
#     rh0  = rh['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0
#     ppt0 = ppt['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0d = pptd3['10/1/%s 0:00'%(y-1):'9/30/%s 23:00'%y] # get rh time slice for stn0]
#     ppt0[ppt0.isnull()] = ppt0d[ppt0.isnull()]
#     pind = ppt0>0      # index to times with precip
#     rind = np.isfinite(rh0)
#     ind = pind&rind
#     b_edg = np.linspace(0, 1, num=51)       # bin edges
#     width = 0.02
#     wts = ppt0[ind].dropna()
#     whst = np.histogram(rh0[ind].dropna(),bins = b_edg,weights=wts)  # get weighted histogram bar heights
#     ax1 = fig4.add_subplot(7,2,y-1997)
#     rects1 = ax1.bar(whst[1][0:np.size(whst[0])]+.01,whst[0], width, color='green') 
#     plt.axis([0,1.01,0,plt.ylim()[1]])
#     plt.title('WY%s R.H.'%(y),y=.83, x=0.4)
#     plt.grid(True)
# #     plt.show()

rh = rh.clip_upper(1) # clip all of the values greater than 1

## update the netcdf file with the corrected data
rn = nc.Dataset(rnc, 'r+')            # create precip ncdf file
rvar = rn.variables["relative_humidity"]
rvar[:,r_list.index(stn0)] = np.array(rh)
 
# a = rvar[:,r_list.index(stn0)]
# plt.plot(a)
rn.close()







