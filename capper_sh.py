'''
20161130 Scott Havens
Quick cap of wind for certain timesteps
20161202 Pat Kormos modified
cap rh at 1
'''


import netCDF4 as nc
import numpy as np
import progressbar

time_steps = range(0,8760)
max_value = 1
# min_value = 0.5


data = '/Volumes/bunnyhill/HUMIDITY_WORK_2016/rh_wy1997.nc'

n = nc.Dataset(data, 'r+')
pbar = progressbar.ProgressBar(np.shape(time_steps)[0]).start()
# pbar = progressbar.ProgressBar(max_value=len(time_steps))
j = 0
for i in time_steps:
	v = n.variables['relative_humidity'][i,:,:]
	v[v >= max_value] = max_value
	# v[v <= min_value] = min_value

	n.variables['relative_humidity'][i,:,:] = v
	
	j += 1
	pbar.update(j)
pbar.finish()

n.close()
